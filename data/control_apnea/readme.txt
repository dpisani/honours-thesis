EEG Data

- control_apnea_*.mat contains
	- colheaders : cell array specifying which columns of the variable 'data' correspond to each electrode
	- data : matrix where rows correspond to samples, and columns correspond to electrodes (mV)
	- rate : sampling rate (Hz) 
	- t : vector of elapsed times corresponding to each row of data
	- epoch_valid : whether the 30 second epoch did not contain highly abnormal artifacts, so clean epochs have a value of 1. In general, the last epoch of data may be incomplete or missing e.g. 960*30s of EEG data, but 931 epochs of epoch_valid so the last entry epoch_valid(end) is usually 0. This variable will largely indicate the presence of movement/muscle artifacts
	- epoch_score : Sleep stage R=REM, W=wake, S1-4= Sleep stage 1-4, which are light sleep, moderately deep sleep (with k-complexes and sleep spindles), and deep sleep (3 and 4) respectively
	- cz_spindles : We have detected sleep spindles in the Cz electrode channel between the indices specified in this matrix. The start index is the first column, the stop index is the second column. So for example, the time series segment corresponding to the first detected sleep spindle is data(cz_spindles(1,1):cz_spindles(1,2),4)
	The sleep spindles have a characteristic frequency around 11-16 Hz, the exact frequency depends on the subject