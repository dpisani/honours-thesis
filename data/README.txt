The physionet extended sleep-edf data set can be found at
http://physionet.org/physiobank/database/sleep-edfx/

This can be simply downloaded by running:

wget -r -np -nH --cut-dirs=2 --accept edf,hyp http://physionet.org/physiobank/database/sleep-edfx/

in this directory
