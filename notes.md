A Subject Specific Approach to Sleep Stage Classification
========================================================

##TODO##
 - appendix
 - thanks

## Method

### Data Processing

 - EEG signals filtered with an eigth order butterworth bandpass filter with cutoff frequency of 0.5-30Hz (pan et al)
 - Epoch divided into 15 2s segments and run under FFT. these were averaged to represent the 30s epoch
 - Power spectrum: FFT summed over 0-30Hz
 - Spectral frequency: mean frequency of spectral power
 - Power ratio: ratio of each band's summed power to the total power



 We wish to assert the following:
  1. Sleep stage classification performs better when the classifier is tailored to the individual being studied.
  2. Individuals do indeed have an EEG fingerprint that distinguishes them amongst others, and that it is possible to determine this fingerprint using signal processing/statistical methods.
  3. It is possible to find similarities between the EEG patterns/fingerprints of people, and that clustering of individuals is therefore also possible.
  4. A classifier trained on similar people will perform better than a classifier trained on a random cluster of the same size
  5. We performed worse than if we simply trained on all our data, but if we had a larger and more diverse group of subjects, we are likely to see an improvement.

 Result Structure
-----------------

  1. Baseline evaluation
     - 10 fold cross validation results on all data
     - 10 fold cross validation results on each subjects data (subject dependant test)
  2. Patient grouping analysis
     - Patient clusters compared with patient fingerprints
     - Davies-Bouldin index, Silhouette index, Calinski-Haberasz index
  3. Clustered classification results
     - Random clustered results (baseline)
     - Fingerprint clustered results (our attempt)
     - k=1 clustering results (train on everything except our test patient)


Discussion
----------

 - fingerprint
    - fft on whole sleep - studies only used NREM
    - have to wait for a whole night before classification can occur - can't perform on-line analysis

 - assumptions:
   - similar people will classify each other better
   - the fingerprints accuratly reflect a persons physiology and the nature of the PSG waveform
   -  a persons physiology directly affects the way the epochs in their sleep are classified
   - is clustering the best way?

 - extensions
   - use clustering to identify people for which manual inspection is necessary
   - try different classifiers/clustering algos
   - try different features
   - cross lab classification - train data from one lab to classify data from another?
   - find more varied data sources
