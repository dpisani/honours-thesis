for i in 1..9
  puts %{\\begin{subfigure}[b]{0.5\\textwidth}
        \\includegraphics[width=\\textwidth]{images/appendix/CA_del_#{i}.jpg}
        \\caption{Night #{i}}
\\end{subfigure}#{'%' unless i % 2 == 0}
}
end
