\chapter{Results and Analysis} \label{chap:results}

In Chapter \ref{chap:subspec}, we created a classifier that aims to be subject specific. In the process of analysing it we must also analyse the aspects which have built up our motivation to make such a classifier in the first place. Namely, we will attempt to assess whether subject specific classification yields better results than subject independent classification, whether it is possible to extract an EEG marker for an individual, and finally whether we can use these markers to provide information to a classifier that will generate an improvement in performance. We will assess these using both databases.

\section{Feature Selection}
It is pertinent to first observe the output from performing sequential forward selection using the SFS algorithm on both the epoch
features and fingerprint vectors of both databases. 

The selected epoch features are shown in Table 6.1. We can see that In all cases the number of features is reduced heavily following the selection. For database $A$,  8 features were selected, all from the frequency domain.  This means that the frequency features were found to be most informative for the classifier. This is consistent with the human sleep scoring which also focuses on detecting frequency patterns. For database $B$, the majority of selected features are also from the frequency domain, but some linear features were chosen as well. The inclusion of non-EEG channels appears to be useful, with 3 EOG features and 1 EMG feature being chosen. It is suspected that the disproportion in this case is due to the fact that the EOG channel is sampled at a much higher rate than the EMG channel, and thus more information can be gained from it.

\begin{table}
    \begin{subtable}[b]{0.4\textwidth}
    		\centering
        \begin{tabular}{l l}
        \hline
        Channel & Features\\
        \hline
        C$_{3}$ & \\
        \hline
		C$_{4}$ &  \\
		\hline
		F$_{z}$ &  $PR_{\gamma}$\\
		\hline
		C$_{z}$ &  $PR_{\alpha}$, $PR_{\delta}$, $PR_{\theta}$\\
		\hline
		P$_{z}$ &  $PR_{\gamma}$, $SP_{total}$\\
		\hline
		O$_{z}$ &  $PR_{\delta}$, $PR_{\theta}$\\
        \end{tabular}
        \caption{Database $A$}
    \end{subtable}
    ~
    \begin{subtable}[b]{0.5\textwidth}
        \centering
        \begin{tabulary}{\textwidth}{l L}
        \hline
        Channel & Features\\
        \hline
        F$_{pz}$-C$_{z}$ & $PR_{\delta}$, $PR_{\gamma}$, $PR_{\theta}$, $SF$, $H_{LE}$, $SD$, $skew$, $Cpx$ \\
		\hline
		P$_{z}$-O$_{z}$ & $PR_{\alpha}$, $PR_{\delta}$, $PR_{\gamma}$, $PR_{\theta}$ \\
		\hline
		EOG Horizontal & $PR_{\delta}$, $SD$, $H_{shann}$ \\
		\hline
		EMG Submental &  $H_{shann}$\\
        \end{tabulary}
        \caption{Database $B$}
    \end{subtable}
    \caption{Selected epoch features following SFS} \label{tab:sfsepoch}
\end{table}

The features in the fingerprint vectors were reduced to an even greater extent. Table \ref{tab:sfsfinger} shows how in both databases the fingerprints are processed to lie in only two dimensions. Again power ratio features are prioritised in database $A$, perhaps to match their selection in the epoch feature vector. In database $B$ the features are those that reflect the shape of the focused spectra, picking mean frequency in the delta range and our mean gradient feature for the spindle band.

\begin{table}
    \begin{subtable}[b]{0.3\textwidth}
    		\centering
        \begin{tabular}{l l}
        \hline
        Channel & Features\\
        \hline
        C$_{3}$ & \\
        \hline
		C$_{4}$ &  \\
		\hline
		F$_{z}$ &  $PR_{\sigma}$\\
		\hline
		C$_{z}$ &  \\
		\hline
		P$_{z}$ &  $PR_{\sigma}$\\
		\hline
		O$_{z}$ &  \\
        \end{tabular}
        \caption{Database $A$}
    \end{subtable}
    ~
    \begin{subtable}[b]{0.3\textwidth}
        \centering
        \begin{tabulary}{\textwidth}{l L}
        \hline
        Channel & Features\\
        \hline
        F$_{pz}$-C$_{z}$ & $Grad_{\sigma}$ \\
		\hline
		P$_{z}$-O$_{z}$ & $SF_{\delta}$ \\
        \end{tabulary}
        \caption{Database $B$}
    \end{subtable}
    \caption{Selected fingerprint features following SFS} \label{tab:sfsfinger}
\end{table}

\section{Effect of Subject Specific Classifiers} \label{sec:subdep}
Although there is much in literature to suggest that building a classifier for single individuals will yield better results, there has been little experimentation done to validate this notion. Taking lead from Redmond and Heneghan \cite{1597499}, we constructed our base MLP classifier in two ways, one to be completely subject specific and one to be subject independent. The subject independent system is described by Section \ref{sec:subindep}, and also doubles as our representation of a standard baseline approach. The subject independent system presented in Section \ref{sec:subdep} equates to a perfect subject dependant system which has information on each new subject specifically, but is unrealistic in the sense that it must have labelled PSG data of each patient \emph{a priori}, which is not usually possible.

The results can be seen in Table \ref{tab:indepvspec}. The results of the subject independent classifier are fairly consistent with the studies we have outlined previously, with an accuracy in the 80\%-90\% range. Our subject dependant classifier improved upon this by ~2\%, reflecting similar improvements seen by Redmond and Heneghan. The same trend is present for Cohen's kappa, being highest in database $B$ at 0.87 for the subject dependant classifier. We will take this as sufficient confirmation of our hypothesis, in that building a classifier specifically for a particular subject will yield better results than if no regard is taken for the person being analysed. The observation made here generalises to both databases. 

We also find that the classification performance on database $B$ is higher than on database A. This is probably due to the higher number of epochs in database $B$ that can be used to learn a pattern, despite the smaller number of channels. On a per-class level (as shown in Fig. 6.1) we see similar trends as our predecessors, with sleep stage S1 being particularly
difficult to classify due to its low occurrence within sleep.

\begin{table}
\begin{subtable}[b]{0.5\textwidth}
\begin{tabular}{l l l}
 & Subject Independent & Subject Specific \\
 Accuracy & 82.82\% & 84.49\% \\
 Cohen's $\kappa$ & 0.762 & 0.785
\end{tabular}
\caption{Database $A$}
\end{subtable}
\begin{subtable}[b]{0.5\textwidth}
\begin{tabular}{l l l}
 & Subject Independent & Subject Specific \\
 Accuracy & 89.50\% &  91.45\% \\
 Cohen's $\kappa$ & 0.835 & 0.866
\end{tabular}
\caption{Database $B$}
\end{subtable}
\caption{Performance of subject specific and subject dependent classification} \label{tab:indepvspec}
\end{table}

\begin{figure}
	\begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/ca_classerr_dep.jpg}
                \caption{Database $A$}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/se_classerr_dep.jpg}
                \caption{Database $B$}
        \end{subfigure}
    \caption{Porportion of errors per class}\label{fig:classerrdep}
\end{figure}

\section{Towards an EEG Identity}
One of our main aims is to provide a means of distinguishing two individuals based on EEG alone. Although we have reason to believe that this is possible by examining the power spectra in certain bands, we must confirm this by examining this phenomena in our own databases. We first conduct visual examination of whole-night spectral density plots in the delta and sigma ranges for the nights in both databases. 

Upon inspection some things are immediately apparent, especially in the sigma band. Firstly, in line with what has been described in Section \ref{sec:litfingerprints}, we commonly see either one or two peaks at around 11Hz and 13Hz within each night. In most cases all channels in the night are shaped the same, although not always positioned at the same height. Some examples are displayed in Figures \ref{fig:sigplotsCA} and \ref{fig:sigplotsSE}. We can observe even more interesting patterns in database $B$, where we have two nights per subject. For the majority of subjects, night 1 and night 2 have a strikingly similar shape in sigma power spectra. On an inter-subject level we see that some individuals have similarly shaped spectra.

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/spec_sig_ca_6.jpg}
                \caption{Night 6}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/spec_sig_ca_7.jpg}
                \caption{Night 7}
        \end{subfigure}
        \caption{Examples of sigma band power spectra in database $A$}\label{fig:sigplotsCA}
\end{figure}

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/spec_sig_se_11.jpg}
                \caption{Night 11}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/spec_sig_se_27.jpg}
                \caption{Night 27}
        \end{subfigure}
        \caption{Examples of sigma band power spectra in database $B$}\label{fig:sigplotsSE}
\end{figure}


Although a visual analysis has provided us with an intuitive sense of the relationships between individuals and their EEG characteristics, we intend to discover a quantifiable metric to describe the similarities we have discussed. We have calculated the two measures described in Section \ref{sec:similarity} on all pairs of nights to help establish how these criteria correspond to what we have determined visually. We have arranged the scores as a matrix where each element $X_{ij}$ is the similarity between night $i$ and $j$ according to the metric used. We observe, as illustrated in Figure \ref{fig:sigsimilarityCA}, that both of our measures are generally in agreement, with a slight difference in the scale and magnitude between Fr\'{e}chet distance and MSE scores. For both databases, both measures agreed on which was the most similar and the most dissimilar pair. We have included these in Figures \ref{fig:sigpairsCA} and \ref{fig:sigpairsSE} to illustrate how this reflects what we find in a visual inspection. In database $A$ the nights with the smallest MSE and Fr\'{e}chet distance have a very similar shape, sharing a peak at 13.5Hz. Conversely, the pair with the highest scores and thus the least similarity are vastly different with peaks at different places. The scores for database $B$ follow suit, with the closest pair being almost identical and the most dissimilar pair having completely different shapes. Since both metrics appear to be in good agreement with each other and with our intuitive notion of similarity, we will use both to quantify the similarity between nights in our analysis.

\begin{figure}[b]
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/ca_sig_frechet_matrix.jpg}
                \caption{Fr\'{e}chet distance}
                \label{fig:sigfrechetCA}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/ca_sig_mse_matrix.jpg}
                \caption{Mean Squared Error}
                \label{fig:sigmseCA}
        \end{subfigure}
        \caption[Similarity matrices for database $A$]{Similarity matrices for database $A$. Darker shades indicate higher similarity.}\label{fig:sigsimilarityCA}
\end{figure}


\begin{figure}
        \centering
        \begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.9\textwidth]{images/spec_sig_ca_1v5.jpg}
                \makebox[20pt][c]
                {
                \raisebox{60pt}[0pt][0pt]{
                \begin{tabular}{r l}
                MSE & 0.027 \\
                Fr\'{e}chet & 0.317 \\
                \end{tabular}
                }
                }
                \caption{Selected Best Pairing: Nights 1 and 5}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.9\textwidth]{images/spec_sig_ca_2v6.jpg}
           \makebox[20pt][c]
                {
                \raisebox{60pt}[0pt][0pt]{
                \begin{tabular}{r l}
                MSE & 0.716 \\
                Fr\'{e}chet & 1.863 \\
                \end{tabular}
                }
                }
                \caption{Selected Worst Pairing: Nights 2 and 6}
        \end{subfigure}
        \caption[Sigma spectra from the most similar and most dissimilar pairs of nights in database $A$]{Sigma spectra from the most similar and most dissimilar pairs of nights in database $A$, with their corresponding MSE and Fr\'{e}chet distance scores.}\label{fig:sigpairsCA}
\end{figure}

\begin{figure}
        \centering
        \begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.9\textwidth]{images/spec_sig_se_21v22.jpg}
                \makebox[20pt][c]
                {
                \raisebox{60pt}[0pt][0pt]{
                \begin{tabular}{r l}
                MSE & 0.001 \\
                Fr\'{e}chet & 0.053 \\
                \end{tabular}
                }
                }
                \caption{Selected Best Pairing: Nights 21 and 22}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.9\textwidth]{images/spec_sig_se_7v13.jpg}
           \makebox[20pt][c]
                {
                \raisebox{60pt}[0pt][0pt]{
                \begin{tabular}{r l}
                MSE & 0.894 \\
                Fr\'{e}chet & 1.125 \\
                \end{tabular}
                }
                }
                \caption{Selected Worst Pairing: Nights 7 and 13}
        \end{subfigure}
        \caption[Sigma spectra from the most similar and most dissimilar pairs of nights in database $B$]{Sigma spectra from the most similar and most dissimilar pairs of nights in database $B$, with their corresponding MSE and Fr\'{e}chet distance scores.}\label{fig:sigpairsSE}
\end{figure}


In the delta band we notice different patterns. Instead of a variety of shapes in the power spectrum we see a single common pattern, a skewed curve with a single peak at approximately 0.5Hz. Despite this, we can see some trends when inspecting the magnitude of this curve. In a similar vein as for the sigma band, we can see that for both nights in each of database $B$'s subjects the height of the peak is very similar, yet across patients there is a range of different heights. This would indicate that for this band it is more important to generate features that reflect the power value in the delta band as opposed to its shape. Indeed, in database $A$ it is only the delta power ratio feature which has been chosen by the SFS algorithm, which indicates that is it a metric of importance. We again apply our MSE and Fr\'{e}chet distance metrics on the delta band, and although the measures are not unanimous on the best and worst pairings, they generally agree on the similarity between nights.

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/spec_del_ca_4.jpg}
                \caption{Database $A$: Night 4}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/spec_del_se_3.jpg}
                \caption{Database $B$: Night 3}
        \end{subfigure}
        \caption{Examples of delta band power spectra in both databases}\label{fig:delplots}
\end{figure}

Since in database $B$ we have two nights per subject, we take the time to compare the intra-individual similarity between these nights. As seen in Table \ref{tab:nightdiff}, nights of the same subject are remarkably similar compared with the average similarity over the whole database. This confirms our notion of a subject having a distinct signature, and shows how this signature is consistent across nights.

\begin{table}
    		\centering
        \begin{tabular}{r l l}
        \hline
        Measurement & Intra-Subject & Overall \\
        \hline
        MSE$_{\sigma}$ & 0.080 & 0.179 \\
        Fr\'{e}chet$_{\sigma}$ & 0.273 & 0.743 \\
        MSE$_{\delta}$ & 1.900 & 3.942 \\
        Fr\'{e}chet$_{\delta}$ & 2.174 & 4.116 \\
        \end{tabular}
    \caption{Average similarity between nights of the same subject and between all nights in database $B$} \label{tab:nightdiff}
\end{table}

With our notion of inter-night similarity established we can now explore how the fingerprint vector that has been created compares in its power to distinguish between individuals.  To this end we create an additional similarity matrix $Y$, with $Y_{ij}$ being the Euclidean distance between fingerprint vectors $f_{i}$ and $f_{j}$. The similarity matrices are all normalised so that all elements lie in the [0,1] range. We then determine the disparity between $Y$ and the MSE and Fr\'{e}chet distance by computing the absolute error for each element in the relevant matrices. The overall level of difference is listed in Table \ref{tab:difffingerprint}. In general we see a 20\% disagreement between our metrics and what is predicted by fingerprint distance, with the Fr\'{e}chet distance usually being a slightly closer measure. We consider this to be an acceptable level of discrepancy to proceed further using the fingerprints we have devised.

\begin{table}
    \begin{subtable}[b]{0.5\textwidth}
    		\centering
        \begin{tabular}{r l l}
        \hline
        Measurement & Mean & Standard Deviation \\
        \hline
        MSE$_{\sigma}$ & 0.202 & 0.128 \\
        Fr\'{e}chet$_{\sigma}$ & 0.219 & 0.208 \\
        MSE$_{\delta}$ & 0.308 & 0.239 \\
        Fr\'{e}chet$_{\delta}$ & 0.279 & 0.257 \\
        \end{tabular}
        \caption{Database $A$}
    \end{subtable}
    ~
    \begin{subtable}[b]{0.5\textwidth}
        \centering
        \begin{tabular}{r l l}
        \hline
        Measurement & Mean & Standard Deviation \\
        \hline
        MSE$_{\sigma}$ & 0.182 & 0.161 \\
        Fr\'{e}chet$_{\sigma}$ & 0.178 & 0.139 \\
        MSE$_{\delta}$ & 0.195 & 0.170 \\
        Fr\'{e}chet$_{\delta}$ & 0.193 & 0.163 \\
        \end{tabular}
        \caption{Database $B$}
    \end{subtable}
    \caption{Difference between fingerprint distance and the similarity measures} \label{tab:difffingerprint}
\end{table}

\section{Clustering Analysis}

The main use of these fingerprints, however, is for the purposes of clustering nights together. Ideally, the similarity between nights within a cluster would be higher than the similarity of nights between clusters. We shall use the metrics explored above to measure whether this is indeed the case with our data sets. Before this can be done we must perform our assessment of the parameter $k$ to use in the k-means procedure. We have computed the indices detailed in Section \ref{sec:clusterindex} for $k \in [2,\frac{N}{2}]$. As expected we observe, in Figures \ref{fig:indexplotsCA} and \ref{fig:indexplotsSE}, that increasing the number of clusters yields a steady improvement in most of the indices. However we note that moving to 4 clusters provides good improvement over having fewer clusters, and yet is a small enough amount to remain useful for our classifier. This is true for both databases, and so we choose $k=4$ for this study.

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/vrc_ca.jpg}
                \caption{VRC Index}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/db_ca.jpg}
                \caption{DB Index}
        \end{subfigure}
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/sil_ca.jpg}
                \caption{Silhouette Index}
        \end{subfigure}
        \caption[Clustering indices from k=2-5 in database $A$]{Clustering indices from k=2-5 in database $A$. The best value is represented by a filled circle.}\label{fig:indexplotsCA}
\end{figure}

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/vrc_se.jpg}
                \caption{VRC Index}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/db_se.jpg}
                \caption{DB Index}
        \end{subfigure}
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/sil_se.jpg}
                \caption{Silhouette Index}
        \end{subfigure}
        \caption[Clustering indices from k=2-18 in database $B$]{Clustering indices from k=2-18 in database $B$. The best value is represented by a filled circle.}\label{fig:indexplotsSE}
\end{figure}

To show the effect that this choice has on the partitioning of the two databases, we have run the k-means algorithm on the fingerprints from both databases. These are displayed in Figure \ref{fig:clustering}. We notice several things. Firstly although the distribution is far from random, we see very few distinct and compact clusters. Naturally, this somewhat impacts the efficacy of a partitioning algorithm such as k-means. Secondly we see, especially in database $A$, that some clusters are merely singletons, and effectively act in separating outlier nights from the rest of the database. We foresee that smaller clusters will suffer in our proposed classification system since there will be less examples to train the classifier. However, separating these starkly different nights may help in preventing unusual data from affecting the classification of the other nights in the database.

\begin{figure}
        \centering
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/cluster_ca.jpg}
                \caption{Database $A$}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/cluster_se.jpg}
                \caption{Database $B$}
        \end{subfigure}
        \caption[Clustering of fingerprints]{Clustering of fingerprints. Lines show boundaries of the Voronoi cells for each cluster.}\label{fig:clustering}
\end{figure}

We used the clustering produced in Figure \ref{fig:clustering} in evaluating the within-cluster similarity of nights according to our similarity criteria. For each cluster, barring those with only a single member, we computed our similarity scores for all pairs of nights within the group. We also computed the pairwise similarity across all nights. The scores are shown in Table \ref{tab:clustercorr}. Although for many clusters the points are more similar to each other than to the database as a whole, the cluster similarity is less substantial than desired. This may be in part related to our earlier observation, in that there do not appear to be any obvious and compact clusters in our fingerprints, and therefore our partitioning is less decisive in bringing together similar nights.

\begin{table}
\begin{subtable}[b]{\textwidth}
        \centering
        \begin{tabular}{l l l l l l}
        \hline
        Group & Size & Mean Fr\'{e}chet$_{\sigma}$  & Mean Fr\'{e}chet$_{\delta}$ & Mean MSE$_{\sigma}$ & Mean MSE$_{\delta}$ \\
        \hline
        1 & 3 & 0.374 & 2.041 & 0.028 & 1.754 \\
        2 & 1 & -- & -- & -- & -- \\
        3 & 4 & 0.789 & 0.227 & 0.104 & 7.009\\
        4 & 1 & -- & -- & -- & -- \\
        All & 9 & 0.743 & 4.116 & 0.179 & 3.942
        \end{tabular}
        \caption{Database $A$}
    \end{subtable}
    
    \begin{subtable}[b]{\textwidth}
    		\centering
        \begin{tabular}{l l l l l l}
        \hline
        Group & Size & Mean Fr\'{e}chet$_{\sigma}$  & Mean Fr\'{e}chet$_{\delta}$ & Mean MSE$_{\sigma}$ & Mean MSE$_{\delta}$ \\
        1 & 15 & 0.343 & 2.071 & 0.076 & 1.025 \\
        2 & 4 & 0.419 & 2.003 & 0.121 & 1.529 \\
        3 & 16 & 0.409 & 3.693 & 0.088 & 3.179 \\
        4 & 1 & -- & -- & -- & -- \\
        All & 36 & 0.421 & 3.011 & 0.113 & 2.402
        \end{tabular}
        \caption{Database $B$}
    \end{subtable}
    
    
    \caption[Average similarity scores within each cluster]{Average similarity scores within each cluster, as well as over all fingerprints. Lower scores indicate higher degrees of similarity.} \label{tab:clustercorr}
\end{table}

\section{Clustered Classification Performance}

With our clustering system in place we can now use our proposed clustered classifier on our databases. To evaluate its performance we compared it to two other classification systems described in Section \ref{sec:testconf}. The first performs clustering on a random basis, while the second doesn't cluster at all. These will help evaluate, respectively, whether the fingerprints we established provide substantial information to our system and whether our clustered method has caused any impact on classification performance compared to a more standard mode of classification.

Overall results are shown in Table \ref{tab:overall3way}. We find again that the results from database $B$ are better than those from $A$, with an accuracy of 86\% over 74\%. Since the randomly clustered classifier operates on similar sized partitions of data as our approach does, we consider it to be a baseline implementation. When we compare the subject clustered classifier to this baseline, we see that we produce improvements over both databases and that this is a statistically significant change (Mann-Whitney $U$. Database $A$: $p=0.015$; database $B$: $p=0.0000036$). Moreover we observe that our improvements are much stronger in database $B$, where there are more nights for which to use in our classifier, creating an improvement of 0.54\% as opposed to the 0.24\% improvement from $A$. This is likely because having more nights increases our ability to find groups of similar people, which strengthens our system. We also compare with the non-clustered classifier. In this case, the advantage of having an MLP classifier trained on the entire database of examples outweighs the advantage of having an understanding of subject similarity. In both databases the difference is statistically significant (Mann-Whitney $U$. Database $A$: $p=5.7\times 10^{-12}$; Database $B$: $p=2.3\times 10^{-21}$). Per-class, we see the same trends in Figure \ref{fig:err3way} as the classifiers in Section \ref{sec:subdep}. What is notable however is that our clustered classifier yields better accuracy for S1 than other classifiers. It is quite possibly the case that there is more inter-subject variation during this stage of sleep, and our subject specific classifier helps narrow down these variations to aid performance.

\begin{table}
\begin{subtable}[b]{\textwidth}
        \centering
        \begin{tabular}{r l l l}
         & Random Clustered & Non Clustered & Subject Clustered \\
        \hline
        Mean Accuracy & 74.15\% & 80.83\% & 74.39\% \\
        Mean $\kappa$ & 0.629 & 0.734 & 0.650 \\
        Mean Training Time & 1.321s & 1.586s & 1.326s \\
        Mean Classification Time & 0.013s & 0.012s & 0.013s
        \end{tabular}
        \caption{Database $A$}
    \end{subtable}
    
    \begin{subtable}[b]{\textwidth}
    		\centering
        \begin{tabular}{r l l l}
         & Random Clustered & Non Clustered & Subject Clustered \\
        \hline
        Mean Accuracy & 85.73\% & 87.79\% & 86.27\% \\
        Mean $\kappa$ & 0.777 & 0.808 & 0.785 \\
        Mean Training Time & 4.635s & 12.194s & 4.705s \\
        Mean Classification Time & 0.013s & 0.012s & 0.013s
        \end{tabular}
        \caption{Database $B$}
    \end{subtable}
     
    \caption{Performance measures over three classification methods.} \label{tab:overall3way}
\end{table}

\begin{figure}
	\begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/ca_classerr_3way.jpg}
                \caption{Database $A$}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/se_classerr_3way.jpg}
                \caption{Database $B$}
        \end{subfigure}
    \caption{Porportion of errors per class}\label{fig:err3way}
\end{figure}

We take a deeper look by investigating performance on a per-night basis. Figures \ref{fig:diffca} and \ref{fig:diffse} show the difference in performance between classifiers for each night in our databases. When comparing to the baseline classifier, our proposed method provides better accuracy for the majority of nights, but for a few subjects severely degrades it. When looking towards the non-clustered classifier, we find that we almost always have less accuracy using our technique, although there are a select few nights in database $B$ that receive good improvement. We can observe the spread in night classification accuracy in Figures \ref{fig:histk4}, \ref{fig:histk4r} and \ref{fig:histk1}. In most cases the distribution is negatively skewed, particularly for our proposed classifier. It is likely that this is because the majority of nights have a good amount of similar subjects to learn from, but those few that are vastly different from the rest do not have this benefit and classification accuracy suffers as a result.

\begin{figure}
	\begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.4\textwidth]{images/diff_ca_k4vk4r.jpg}
                \makebox[120pt][c]
                {
                \raisebox{80pt}[0pt][0pt]{
                \begin{tabular}{r r}
                Mean & + 0.26\% \\
                Standard Deviation & 4.45\% \\
                \end{tabular}
                }
                }
                \caption{Random Clustered}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.4\textwidth]{images/diff_ca_k4vk1.jpg}
                \makebox[120pt][c]
                {
                \raisebox{80pt}[0pt][0pt]{
                \begin{tabular}{r r}
                Mean & - 0.43\% \\
                Standard Deviation & 5.93\% \\
                \end{tabular}
                }
                }
                \caption{Non-Clustered}
        \end{subfigure}
    \caption{Difference in accuracy between Subject Clustered Classification and alternative methods for database $A$.}\label{fig:diffca}
\end{figure}

\begin{figure}
	\begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.4\textwidth]{images/diff_se_k4vk4r.jpg}
                \makebox[120pt][c]
                {
                \raisebox{80pt}[0pt][0pt]{
                \begin{tabular}{r r}
                Mean & + 0.66\% \\
                Standard Deviation & 1.73\% \\
                \end{tabular}
                }
                }
                \caption{Random Clustered}
        \end{subfigure}%
        \\
        \begin{subfigure}[b]{\textwidth}
                \includegraphics[width=0.4\textwidth]{images/diff_se_k4vk1.jpg}
                \makebox[120pt][c]
                {
                \raisebox{80pt}[0pt][0pt]{
                \begin{tabular}{r r}
                Mean & - 1.37\% \\
                Standard Deviation & 2.66\% \\
                \end{tabular}
                }
                }
                \caption{Non-Clustered}
        \end{subfigure}
    \caption{Difference in accuracy between Subject Clustered Classification and alternative methods for database $B$.}\label{fig:diffse}
\end{figure}


\begin{figure}
	\begin{subfigure}{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/ca_hist_k4.jpg}
                \caption{Database $A$}
        \end{subfigure}%
        \begin{subfigure}{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/se_hist_k4.jpg}
                \caption{Database $B$}
        \end{subfigure}
    \caption{Histogram of classification accuracy per night via Subject Clustered Classification}\label{fig:histk4}
\end{figure}

\begin{figure}
	\begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/ca_hist_k4r.jpg}
                \caption{Database $A$}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/se_hist_k4r.jpg}
                \caption{Database $B$}
        \end{subfigure}
    \caption{Histogram of classification accuracy per night via Random Clustered Classification}\label{fig:histk4r}
\end{figure}

\begin{figure}
	\begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/ca_hist_k1.jpg}
                \caption{Database $A$}
        \end{subfigure}%
        \begin{subfigure}[b]{0.5\textwidth}
                \includegraphics[width=\textwidth]{images/se_hist_k1.jpg}
                \caption{Database $B$}
        \end{subfigure}
    \caption{Histogram of classification accuracy per night via Non-Clustered Classification}\label{fig:histk1}
\end{figure}

\section{Reflection}
After making the above observations, we conclude that our Subject Clustered Classifier gains real advantages by utilising information about individual subjects. However, when applied to our databases we found that the method also has its weaknesses that prevent it from immediately becoming a \emph{de facto} standard for the clinical domain. Addressing these would greatly influence the validity of our method.

Core to our technique is the creation of a representation of a subject in terms of their sleep patterns. Based on what we discovered in \ref{sec:litfingerprints}, we chose to focus our attention on a small aspect of sleep EEG, namely the power spectrum in the sigma and delta frequency ranges. While we have shown this to be a good discriminator between subjects, there are some areas that require consideration. Firstly, the work by De Gennaro et al. \cite{DeGennaro2005114} that introduces the concept of an electroencephalographic fingerprint only extracts information from non-REM sleep. Since in our case we cannot know which parts of our signal are non-REM sleep we opt to use the entire signal, which inevitably contains both REM and waking data. This may have the effect of introducing artefacts which could harm the usefulness of our fingerprints. Secondly, although the features extracted for our fingerprint vectors are considered good enough for use, there needs to be more investigation into other metrics that could be used to effectively capture the essence of the EEG waveform patterns that undeniably exist. We have introduced the Fr\'{e}chet distance and MSE as similarity measures, and we have shown that these are effective metrics for use. With these established it is now possible to evaluate other types of fingerprint features.

Additionally, we have seen that most of the time fingerprints do not form compact clusters. Therefore using k-means may not have been the best choice to partition nights apart. Other clustering algorithms can be explored and we might see better results with a better way of grouping similar people. 

Our notion of a sleep fingerprint is a whole-night summary of a person's sleep. With it we can perform classification once a subject sleeps a single night under study. This is useful in most circumstances. In other cases where a more on-line analysis is required, an adaptation needs to be made to generate a fingerprint using a smaller segment of sleep data.

Above all what is clear is that the quality of the databases themselves have had the biggest impact upon classification performance. We have repeatedly observed that database $A$, having far fewer nights than database $B$, performs significantly worse during classification. Our method relies on there existing groups of nights that are similar to one another. However, with only 9 nights, this is not possible with database $A$, and therefore the improvement we see in our proposed classifier over the baseline is minimal. Once we gain more nights, as in database $B$, we get better results over the baseline. We are led to conclude that there is cause to continue working with a Subject Clustered Classifier, but only when it is possible to operate on a database that is significantly larger.

\section{Summary}
In this chapter we have discovered several things. Firstly, we found that classifiers that pay no regard to the individual from which a sample comes from will perform worse than a classifier that has supreme knowledge of the subject being analysed. We assessed the inter-subject differences between subjects, and found that we can use distance metrics to quantify the similarity between different nights of sleep. With these tools we were able to examine our fingerprint vectors, and found that they were an acceptable representation of a night's sleep for the purposes of distinguishing subjects. Finally by comparing our Subject Clustered Classification with other methods we show that we surpass the baseline by utilising this fingerprint, but with our data sets we did not reach the performance of a non-clustering classifier. Following this investigation, we come to assert that our proposed method has many merits for use in automatic sleep stage classification, but to fully examine them we first need a more comprehensive database consisting of many recordings of many subjects.