\chapter{Automated Sleep Staging} \label{chap:litreview}

As humans, we unavoidably spend a significant portion of our lives asleep. Proper sleep is vital for wellbeing, and those who suffer from disturbed sleep are likely to find themselves also suffering from frequent mental distress, depressive symptoms and anxiety \cite{Strine200523}, and can expect a degradation in cognitive and motor performance \cite{pilcher1996effects}. Sleep insufficiency also has physical effects on the body, disrupting bodily processes \cite{Spiegel19991435} and causing pain \cite{Strine200523}. Analysis of polysomnographs (PSGs) play a vital role in diagnosis and treatment of sleep conditions. A major part of this analysis is the classification of an individual's sleep into several stages, in accordance with the set of rules put in place by Rechtschaffen and Kales (R\&K) \cite{rechtschaffen1968manual}. This is a laborious process, involving the manual inspection of an entire sleep recording and the labeling of each 30 second segment. An automation of this process is thus desirable, in order to relieve clinicians of this task as well as provide a reliable and consistent means of classification.

A significant effort has been made to apply machine learning techniques to the task of automatic sleep staging. These methods involve training a classifier using a set of labelled examples to build a model under which new, unlabelled examples can be classified. For the sleep classification task, the general procedure consists of three stages \cite{6705790}. First, the recorded signals are preprocessed to eliminate artefacts and noise, and split into 30 second epochs. The second step is the extraction of features from each epoch that meaningfully represent it. These are finally given to a classifier which labels the epoch as one of the sleep stages. Numerous methods have been developed in literature which all differ in the implementation of the above mentioned steps.

While these methods accurately classify the majority of samples, there are still are a number of challenges which prevent automatic classifiers from being effective enough for clinical purposes. Many of these arise from the fact that the R\&K system does not take into account micro structures in sleep, and that the scoring rules lend themselves to subjectivity from experts, who often disagree amongst themselves. Additionally, current classification systems do not take into account inter-individual variability in PSG readings when performing analysis, which make fixed rules difficult to use.

In this review we will outline the prevailing R\&K standard for sleep staging, the automated methods that have been employed for the task and the limitations and considerations that are relevant for the development of improved classifiers.

\section{Manual Sleep Staging}

The R\&K rules classify sleep into six stages. These progress in a cyclic manner and range from wake, deep sleep (itself classified into stages S1, S2, S3 and S4), and REM sleep. Typically, the recording used has a minimum of four channels taking measure of brain activity (electroencephalogram, EEG), muscle activation (electromyogram, EMG) and eye movement (electrooculogram, EOG). The manual suggests a single central EEG derivation placed at either C$_{4}$-A$_{1}$ or C$_{3}$-A$_{2}$ using the 10-20 system for EEG electrode placement, noting that regional differences are not consequential to the sleep scoring \cite{silber2007visual}. Two EOG derivations are recommended to record eye movements, and a singular EMG derivation placed under the chin should be used. A primarily visual analysis of each 20-30 second epoch of these readings is then conducted and a label assigned, in order to produce a \emph{hypnogram} for the night. A summary of the R\&K guidelines can be seen in Table \ref{tab:rkscoring}. These guidelines have been in place for over 40 years, and remain the gold standard for sleep analysis and diagnosis. In more recent times it is common to use more than one EEG derivation, and in some cases the suggested placement is disregarded in favour of other channels which prove to be equally as useful.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/hypnogram.jpg}
    \caption{A hypnogram for part of a night}
\end{figure}

\begin{figure}[t]
    \centering
    \includegraphics[width=0.4\textwidth]{images/1020system.png}
    \caption[Electrode placements under the 10-20 system]{Electrode placements under the 10-20 system. \cite{spriggs2014essentials}}
\end{figure}

\begin{table}
\begin{tabulary}{\textwidth}{|l|L|}
\hline
Stage	&	Criteria \\
\hline \hline
Wake		&	>50\% of the page (epoch) consists of alpha (8-13 Hz) activity or low voltage, mixed (2-7 Hz) frequency activity. \\
\hline
S1	&	50\% of the epoch consists of relatively low voltage mixed (2-7 Hz) activity, and <50\% of the epoch contains alpha activity. Slow rolling eye movements lasting several seconds often seen in early stage 1. \\
\hline
S2	&	Appearance of sleep spindles and/or K complexes and <20\% of the epoch may contain high voltage (>75 $\mu$V, <2 Hz) activity. Sleep spindles and K complexes each must last >0.5 seconds. \\
\hline
S3	&	20\%-50\% of the epoch consists of high voltage (>75 $\mu$V), low frequency (<2 Hz) activity. \\
\hline
S4	&	>50\% of the epoch consists of high voltage (>75 $\mu$V) <2 Hz delta activity. \\
\hline
REM	&	Relatively low voltage mixed (2-7 Hz) frequency EEG with episodic rapid eye movements and absent or reduced chin EMG activity. \\
\hline


\end{tabulary}
\caption{Rechtschaffen and Kales Sleep Staging Criteria}\label{tab:rkscoring}
\end{table}


The nature of the EEG waveform is a central factor for classification under the R\&K criteria. Brain activity changes noticeably throughout each of the sleep stages, and follows sets of identifiable patterns. As described by Šušmáková and Krakovská \cite{Susmakova2008}, there exist five basic types of waves to be found in EEG readings.

\subsubsection*{Delta Waves ($\delta$)}

Delta activity occurs during infancy, deep sleep and in those with certain brain diseases. During sleep they are seen most in frontal regions, and appear with a frequency of up to 4 Hz and an amplitude of up to 100 $\mu$V.

\subsubsection*{Theta Waves ($\theta$)}

These have frequency of 4-8 Hz and an amplitude below 100 $\mu$V, and become present in adults during sleep and emotional stress, and are typical for children when awake.

\subsubsection*{Alpha Waves ($\alpha$)}

Are typical for an awake resting state with subjects who have their eyes closed, and contain frequencies between 8-12 Hz. During sleep their amplitude is highest frontally, while during waking periods it is predominant in the occipital region.

\subsubsection*{Beta Waves ($\beta$)}

Beta waves span a frequency of 13-22 Hz or even higher. The region where these meet the alpha range is termed sigma activity.

\subsubsection*{Gamma Waves ($\gamma$)}

Any rhythms beyond the range of beta activity and have an amplitude less than 2 $\mu$V are denoted as gamma waves, and are seen during intense mental activity and some kinds of sensory stimulation.\\

In addition to these fundamental wave types, some EEG events are of significance in the classification of sleep stages. S2 is particularly characterised by events known as sleep spindles and K-complexes. Sleep spindles are bursts of 12-16 Hz activity. A K-complex is a sharp negative wave that is followed by a slower positive wave, and occur randomly throughout S2 \cite{Susmakova2008}. These events should last at least 0.5 seconds, and if they occur successively in a short enough time period the epoch is classified as S2.

\begin{figure}[b]
    \centering
    \includegraphics[width=\textwidth]{images/spindle.jpg}
    \caption{A sleep spindle}
\end{figure}

\section{Building a Classifier}

As mentioned previously, although a multitude of techniques have been applied to the task of sleep stage classification, there are underlying principles in building a classification system that occur in the majority of previous studies. Polysomnography data is collected from a group of subjects which is then labelled by experts. PSG signals tend to be affected by noise and artefacts created by eye movement, body movement and system interferences \cite{bates1972fundamentals}. Therefore these signals are usually preprocessed to normalise the signal and detect artefacts \cite{6705790}. Often the sample is filtered using Butterworth bandpass filters of the 6\textsuperscript{th} to 8\textsuperscript{th} order. Several of these are used, usually filtering bands that correspond to the frequency ranges of alpha, beta, gamma, delta and theta wave types, or the entire range of 0.5-30 Hz. This raw signal is insufficient to train a classification model. Therefore, these signals are analysed in order to extract a vector of significant features that represent each 30 second epoch. A wide variety of features have been explored in literature, most of which are based in the frequency domain, although features from the time domain are also used and in some cases non-linear features are also tested. With these feature vectors a classifier can then be trained using the labelled examples.

\subsection{Feature Extraction}

Automated classification systems generally rely upon a set of computed features that represent the primary attributes of each epoch. Significant effort has been put into producing effective methods of feature generation that precisely encapsulate the events particular to sleep recordings, so that classification methods in turn will more accurately label new samples. Many approaches aim to extract the same factors used in evaluating the R\&K criteria, while other attempts have been made to find alternative measures in EEG, EMG and EOG data which are more useful for automated classification.

\subsubsection{Time Domain Features}
Although not as prevalent as spectral measures, features processed from the signal in the time domain are often selected as being useful for building classifiers \cite{zoubek2008two,ACS:ACS1147,1597499}. Typical metrics include signal entropy, standard deviation, skew, zero crossing rate and a set of three quantitative parameters defined by Hjorth \cite{Hjorth1970306}, namely activity, mobility and complexity. A study by Zoubek et al. found that when selecting relevant features from PSG recordings it was desirable to incorporate some time-domain features for EMG and EOG channels, although it was noted that adding skewness metrics actually harmed classification accuracy \cite{Zoubek2007171}.

\subsubsection{Frequency Analysis}

Since sleep recordings are described in terms of waveforms, signal processing techniques can be applied to sleep data to condense the input into a manageable feature vector. Though there is no one method used to gather spectral features, there are several studies that share common processes \cite{pan2012transition,6121664,5138842,Zoubek2007171,Guenes2010}. Most of these are focused upon the EEG channels in particular, for the purposes of separating the different wave features present in the sample.

Since PSG signals by nature lie in the time domain, Fast Fourier Transforms (FFTs) are commonly used to transform the data from the time domain into the frequency domain. In this domain it is useful to evaluate the total spectral power over the whole frequency domain \cite{pan2012transition}, as well as the relative spectral energies for frequency bands corresponding to $\alpha$, $\beta$, $\delta$, $\theta$ and $\gamma$ waves \cite{zoubek2008two, ACS:ACS1147,1597499,Susmakova2008,Zoubek2007171,pan2012transition}. Processing steps to measure the presence of sleep spindle events, which are vital in classifying S2, have been used by Pan et al. in addition to the above described spectral features \cite{pan2012transition}.

Alternate signal processing mechanisms have been used instead of or in conjunction with the ones described. With the goal of detecting interference artifacts in EEG data, Roháľová et al. employ a Kalman Filter which acts in estimating the state of the system from measurements which contain random errors \cite{rohalova2001detection}. In the Kalman Filter they then embed an autoregressive model, a linear predictor used for modeling stationary time series. This conjunction with the Kalman Filter allows the model to be used for non-stationary signals such as EEG. However, this is shown to be not as effective as a different model which uses neural networks and an Extended Kalman Filter \cite{rohalova2001detection}. Use of a Hilbert-Huang Transform has been shown to improve the classification accuracy for particular sleep stages than the Fourier transform described previously \cite{5138842}, and the feature set made by this process was used to classify with an average accuracy rate of 81.7\% with a nearest neighbor method. Wavelet packet transforms have also been applied onto EEG signals for classification \cite{4649365,Sinha2008}. Wavelet transforms are a tool that promise improved accuracy in locating transient (high frequency) waves while maintaining a good frequency resolution for slow waves \cite{4649365}. While this seems ideal for EEG readings, being a dynamic signal that is prone to spikes and usually corrupted with noise, Wavelet transforms have been shown to be no better than Fourier transforms when the two have been compared using the same data set \cite{Zoubek2007171}, and since they take longer to process than FFT there seems little motivation to use Wavelet transform features. 

\subsubsection{Non-Linear Feature Extraction}
While using linear spectral features remains popular, there are arguments for the adoption of non-linear measures in the classification of EEG signals. There are many facts that indicate that the dynamics of the brain are non-linear in nature, and that many feedback loops exist at the neuronal level \cite{raey}. As described by Fell et al. \cite{Fell1996401}, there are two branches of thought on non-linear EEG analysis that have emerged: an 'ontological branch' and a 'pragmatic branch'. The ontological branch questions whether the data from EEG readings can indeed be sufficiently explained in non-linear terms as opposed to linear stochastic models. Though there is evidence that may suggest this is the case, it still remains an open question, and thus there is no certainty as to whether non-linear features best describe the data being analysed. The second branch of thought described, the pragmatic branch, instead disregards this question. Instead, it contemplates whether these models can reveal useful information that cannot be attained by linear spectral measures alone and which can be useful in EEG analysis. The study then proceeds to compare the effectiveness of linear vs non-linear measures in classifying the R\&K stages, to examine whether this is the case.

Linear features, including those calculated from spectral analysis as well as some from the time domain were chosen in the study. These were contrasted with the non-linear measures of the largest Lyapunov exponent, approximated Kolmogorov entropy and correlation dimension. It was concluded that the non-linear measures added non-redundant information, and aided the discrimination between S1 and S2. However, spectral measures hold advantages in distinguishing S2 from Slow Wave Sleep (R\&K stages S3 and S4 combined). Thus, a combination of both linear and non-linear features is recommended over spectral measures alone.

Non-linear features cannot be used for effective classification of all sleep stages however. Attempts at classifying REM sleep using largest Lyapunov exponent and correlation dimension on neural networks by Grözinger et al. \cite{raey} provide no improvement when combined with spectral power features. Indeed, when used alone the non-linear features performed significantly worse in classification of the used data set. It is then clear that the choice of features used is highly dependent upon the experimental situations and the kinds of EEG data used.

\subsection{Feature Selection}

Of great importance is the selection of which variables should be used for automatic classification. An over abundance of features can impact negatively upon machine learning processes, as unimportant variables can reduce the accuracy of classifiers and unnecessarily increase the computational complexity. A mutual information measure is used by Herrera et al. \cite{6121664} to identify non-linear relations between the variables. The mutual information estimator is designed in such a way to maximise the relevance of features while minimising redundancy between them. The algorithm proposed incrementally ranks all the potential variables, and then selects a subset of the top variables which maximises the classification performance. While an analysis of the results shows that redundant variables were indeed eliminated, the classification done in the study did not surpass the performance of those done in other works. However it was noted that the choice of classifier, as well as the fact that only a single channel was used for classification, could account for the level of accuracy demonstrated and thus the variable elimination procedure itself might still be useful. A more common technique is the Sequential Forward Selection algorithm (SFS) \cite{Pudil19941119}. This method, although suboptimal, aims to incrementally include a subset of features to maximise a certain criterion. This incremental selection of features was performed by Zoubek et al. \cite{Zoubek2007171} using an accuracy measure as criteria for selection. Various classifiers were used to ensure that the processing itself is insensitive to what classifier is used, and overall the selection of features improved the classification of the sleep/wake stages. As it is efficient, effective and easy to analyse, it presents itself as a viable method for use within a range of studies \cite{zoubek2008two,1597499,ACS:ACS1147}.

\section{Classification Methods}

\subsection{Neural Networks}

Artificial neural networks are a common choice of classifier for identification of sleep stages, and have been used with a variety of feature sets \cite{4649365, Schaltenbrand1993157,Principe1993399,Sinha2008,raey,Becq2005}. A description of a neural network system used for sleep stage classification is given by Schaltenbrand et al. \cite{Schaltenbrand1993157}. The model used was a multilayer perceptron. Since single layer perceptrons can only solve linearly separable problems, a hidden layer of perceptrons is necessary to create a non-linear separation boundary suitable for such a complex data set as polysomnography readings. Each feature of the input vector is connected to a neuron on the input layer. These are all fully connected to neurons in the hidden layer, which are themselves fully connected to an output layer, which has a neuron for each of the six R\&K stages. Each of these connections has an associated weight which scales the values given as activation input for each perceptron. Each neuron uses an activation function (typically a sigmoid function) which maps the sum of all inputs to an output value. These weights are trained using a back-propagation algorithm, which is effectively a gradient search function minimising the error between the desired and actual values on the output layer.

Configurations of neural networks differ between implementations. For example, the network described by Schaltenbrand et al. \cite{Schaltenbrand1993157} uses 10 units in the hidden layer for 17 inputs, while another \cite{4649365} uses 8 hidden neurons for 12 inputs. Since there is no method to guarantee which amount of hidden neurons will optimise the classification, the configuration is discovered on a contextual basis.

\subsubsection{Comparisons With Other Methods}

A comparison between the neural networks described above and four other traditional classifiers was conducted to evaluate performance on classifying sleep stages \cite{Becq2005}. Eight features were created from both the time and frequency domain, and were used for classification with linear and quadratic classifiers, k nearest neighbors, Parzen kernels and a neural network classifier. While none of the methods studied achieved the level of accuracy that is seen in human scorers (who typically disagree 10-20\% of the time), they are close to the interval. Neural networks were identified as advantageous in that additional data transformations is not necessary to improve results, they can deal with non-Gaussian probability distributions and can handle extreme values. Additionally they are not as resource consuming as k-nearest neighbor classifiers, which although achieving the best results with homogeneous data require storing large amounts of training vectors in memory.

A separate comparison between a neural network, belief automation and expert system \cite{Principe1993399} supports the idea that automated systems do not yet reach a satisfactory level of agreement with human scorers, but suggests that the similarity in results between the different classification systems indicate that there is a need for enhancing the accuracy of the initial EEG feature extraction as opposed to the need for a better classifier.

\subsection{Hidden Markov Models}

Also gaining popularity is the use of Hidden Markov Models (HMMs) to identify the sequence of sleep stages. Described in the classical text by Rabiner and Juang \cite{1165342}, a HMM model contains a stochastic observable process and an underlying non-observable stochastic process. Such a model can be used to answer three problems:

\begin{enumerate}
\item Given an observation sequence $O=O_{1},O_{2},...,O_{T}$ and the model $\lambda$, compute the probability of the observation sequence $P(O|\lambda)$

\item Given an observation sequence $O=O_{1},O_{2},...,O_{T}$, and the model $\lambda$, determine a state sequence $I=I_{1},I_{2},...,I_{T}$ which is optimal in a meaningful sense.

\item Given an observation sequence $O=O_{1},O_{2},...,O_{T}$, adjust a model $\lambda$ to maximise $P(O|\lambda)$
 .
\end{enumerate}


For the task of sleep staging we are interested in solving problem 2, and Hidden Markov Models appear to be an apt model for EEG analysis since the information is non-stationary and non-localised \cite{Penny1998}. The optimality criteria referred to in this problem is to find the single best state sequence with the highest probability, and there exists a formal procedure for finding this called the Viterbi algorithm \cite{1165342}. For sleep staging, the hidden states in the HMM are the R\&K labels, and the observations are the feature vectors which can be created by any of the means previously described. Experiments using HMM classifiers have proven to provide results in good agreement with experts \cite{Doroshenkov2007}.

A range of variations of the classic Hidden Markov Model have been used for the task of classifying sleep stages. Pan et al. introduce transition constraints upon the model, so that transitions between sleep stages that are known to be impossible or improbable are not chosen. This has significantly enhanced the recognition rate when compared to prior studies \cite{pan2012transition}. Several Coupled Hidden Markov Models have been proposed by Zhong and Ghosh, which are designed to better characterise multiple interdependent sequences \cite{zhong2002hmms}. However, while multivariate HMMs have good classification accuracy, using a more complex CHMM does not necessarily create better results, due to an increased amount of complexity and associated assumptions.


\section{Concerns With R\&K Scoring}

Though the Rechtschaffen and Kales guidelines are the prevailing standard, there is increasing dissatisfaction within the sleep research community as to the validity of R\&K sleep staging \cite{Flexer2005199}. Firstly, it is a very tedious task with room for subjective interpretation in the visual identification of stages. Secondly, the standard is defined in terms of a low 30 second temporal resolution, and accounts for only 6 stages neglecting any smaller structures of sleep \cite{doi:10.1080/088395102753559271}. This causes epochs that lie on the transition of two sleep phases to be more likely misclassified. The manual also only calls for a single optimal central EEG derivation for scoring sleep, and does not take into account regional differences as long as certain types of activity are registered \cite{silber2007visual}. However, there are known topographical aspects of delta activity and sleep spindles which cannot be captured by a single central derivation \cite{Susmakova2008}. It is thus questionable, in an age where over 16 channels can be simultaneously recorded with ease, whether a single derivation is an adequate measure. It should be noted, however, that a study conducted by Flexer et al. find that for the purposes of automated continuous sleep staging a single EEG channel is sufficient to obtain reliable results \cite{Flexer2005199}.


There is also concern with the level of agreement between experts. Since sleep staging is a subjective process done via visual analysis, disagreements between experts is common, occurring approximately 20\% of the time \cite{JSR:JSR700}. Furthermore, the level of disagreement is even higher between different sleep laboratories, which makes analysis between labs difficult \cite{PMID:11083599}. These issues carry over onto automatic classification. Inconsistencies between data sets make it difficult to create a classifier that can be used universally, and training data has to be created for each lab \cite{Flexer2005199}. Alternatives such as the AASM standard have been proposed to replace the R\&K rules, which have improved the agreement between scorers for all stages but one \cite{PMID:11083599}.

\section{Consideration of Inter-Individual Variations}
A major problem in analysing sleep is that the differences in PSG patterns between patients are largely ignored. In most experiments an entire collection of PSG recordings are pooled together before training and evaluation is conducted upon the entire dataset. A notable exception is a study done by Redmond and Heneghan, where subject specific and subject independent classifiers were compared \cite{1597499}. It was found that when using an EEG based classifier, subject specific training yielded results approximately 3\% more accurate than a subject independent classifier. This is not surprising, as PSG recordings are subject to differences in recording conditions and differences in patient physiology. An attempt has been made to mitigate the variability in PSG recordings by selecting the most robust features for classification. However this attempt did not address patient variability specifically, and a larger data set is called for to compensate for such high inter-subject variations \cite{ACS:ACS1147}.

\subsection{A Fingerprint of Sleep} \label{sec:litfingerprints}
There is growing substantial evidence that traits in an individual's EEG is influenced by their genetic make-up and that these traits are retained across nights. Investigation into age and gender differences show that spectral profiles of individuals are highly correlated between waking and sleeping EEG. Although gender had no visible effect on EEG patterns, it was shown that power in the delta band diminished with age \cite{Ehlers1998199}. In addition to differentiations in the delta band, there are distinct patterns to be found in the spectral range encompassing spindle activity during non-REM sleep. Notable is a bimodal pattern with peaks at 11.5 and 13.0 Hz in a number of subjects, and only a single peak in others. These patterns are also consistent across nights for the same subject, and are distinct between individuals \cite{Werth1997535}.

Such distinct individual traits allow for the construction of an encephalographic marker to describe topographic patterns in an individual's non-REM sleep. This \emph{fingerprint} is first described by De Gennaro et al. in an effort to emphasise biological differences in sleep between subjects \cite{DeGennaro2005114}. The sigma range (8.0-15.5 Hz) was used as a focus, and EEG from antero-posterior derivations (F$_{z}$-A$_{1}$, C$_{z}$-A$_{1}$, P$_{z}$-A$_{1}$ and O$_{z}$-A$_{1}$) were collected from a range of subjects over several nights. In line with previous studies, this fingerprint was distinguishing among subjects, and each individual's EEG trait remained substantially invariant across multiple nights, even when the subjects were subjected to different sleeping conditions. A subsequent study found that such a fingerprint is determined by genetic factors, after observing striking similarities in the sleep fingerprints of pairs of twins \cite{ANA:ANA21434}. In addition to sigma activity, features in the sleep hypnogram were also examined, including proportion of REM, S2 and SWS, and latency to S2 and REM stages which also contribute as distinguishing factors. A third study by Tucker et al. adds non-REM power in the delta band as a feature, and found that this is also a good discriminator between individuals \cite{JSR:JSR594}. Additionally, the study found that these sleep variables did not vary between subjects in a completely independent manner, but rather variability tended to cluster in three dimensions representing sleep duration, sleep intensity, and sleep discontinuity.

\section{Summary}

Distinguishing sleep phases from polysomnography readings is a non-trivial task with practical applications. While there have been numerous attempts to provide automated means of sleep staging under the R\&K standard, there has yet been no single method that promises performance at an expert level. Though performance can be improved using classifiers optimised for the task, the correct choice of features proves to be more critical in affecting classification accuracy. Furthermore, it is important to consider that due to the physiological nature of the data being examined, the resulting features that are being extracted are affected by the biological structure of each patient being recorded. It is thus worthwhile to examine whether accounting for these variations will provide any improvement to the current methods of automated sleep staging.