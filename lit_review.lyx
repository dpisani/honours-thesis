#LyX 2.0 created this file. For more info see http://www.lyx.org/
\lyxformat 413
\begin_document
\begin_header
\textclass achemso
\use_default_options true
\begin_modules
theorems-ams
eqs-within-sections
figs-within-sections
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman default
\font_sans default
\font_typewriter default
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100
\font_tt_scale 100

\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry false
\use_amsmath 1
\use_esint 1
\use_mhchem 1
\use_mathdots 1
\cite_engine natbib_numerical
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Literature Review
\end_layout

\begin_layout Author
Dean Pisani
\end_layout

\begin_layout Email
dpis1047@uni.sydney.edu.au
\end_layout

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
As humans, we unavoidably spend a significant portion of our lives asleep.
 Proper sleep is vital for wellbeing, and those who suffer from disturbed
 sleep are likely to find themselves also suffering from frequent mental
 distress, depressive symptoms and anxiety 
\begin_inset CommandInset citation
LatexCommand cite
key "Strine200523"

\end_inset

, and can expect a degrade in cognitive and motor performance 
\begin_inset CommandInset citation
LatexCommand cite
key "pilcher1996effects"

\end_inset

.
 Sleep insufficiency also has physical effects on the body, disrupting bodily
 processes 
\begin_inset CommandInset citation
LatexCommand cite
key "Spiegel19991435"

\end_inset

 and causing pain 
\begin_inset CommandInset citation
LatexCommand cite
key "Strine200523"

\end_inset

.
 Analysis of sleep recordings play a vital role in diagnosis and treatment
 of sleep conditions.
 A major part of this analysis is the classification of an individual's
 sleep into several stages, in accordance with the set of rules put in place
 by Rechtschaffen and Kales (R&K) 
\begin_inset CommandInset citation
LatexCommand cite
key "rechtschaffen1968manual"

\end_inset

.
 This is a laborious process, involving the manual inspection of an entire
 sleep recording and the labeling of each 30 second segment.
 An automation of this process is thus desirable, in order to relieve clinicians
 of this task as well as provide a reliable and consistent means of classificati
on.
\end_layout

\begin_layout Section
Manual Sleep Staging
\end_layout

\begin_layout Standard
The R&K rules classify sleep into six stages.
 These progress in a cyclic manner and range from wake, deep sleep (itself
 classified into stages S1, S2, S3 and S4), and REM sleep.
 Typically, the recording used has a minimum of four channels taking measure
 of brain activity (electroencephalogram, EEG), muscle activation (electromyogra
m, EMG) and eye movement (electrooculogram, EOG).
 A primarily visual analysis of each 20-30 second epoch of these readings
 is then conducted and a label assigned.
\end_layout

\begin_layout Standard
The nature of the EEG waveform is a central factor for classification under
 the R&K criteria.
 Brain activity changes noticeably throughout each of the sleep stages,
 and follows sets of identifiable patterns.
 As described by Šušmáková and Krakovská 
\begin_inset CommandInset citation
LatexCommand cite
key "Susmakova2008"

\end_inset

, there exist five basic types of waves to be found in EEG readings.
\end_layout

\begin_layout Subsection*
Delta Waves
\end_layout

\begin_layout Standard
Delta activity occurs during infancy, deep sleep and in those with certain
 brain diseases.
 During sleep they are seen most in frontal regions, and appear with a frequency
 of up to 4 Hz and an amplitude of up to 100
\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\uuline off
\uwave off
\noun off
\color none
 
\family default
\series default
\shape default
\size default
\emph default
\bar default
\strikeout default
\uuline default
\uwave default
\noun default
\color inherit

\begin_inset Formula $\mu$
\end_inset

V.
\end_layout

\begin_layout Subsection*
Theta Waves
\end_layout

\begin_layout Standard
These have frequency of 4-8 Hz and an amplitude below 100 
\begin_inset Formula $\mu$
\end_inset

V, and become present in adults during sleep and emotional stress, and are
 typical for children when awake.
\end_layout

\begin_layout Subsection*
Alpha Waves
\end_layout

\begin_layout Standard
Are typical for an awake resting state with subjects who have their eyes
 closed, and contain frequencies between 8-12 Hz.
 During sleep their amplitude is highest frontally, while during waking
 periods it is predominant in the occipital region.
\end_layout

\begin_layout Subsection*
Beta Waves
\end_layout

\begin_layout Standard
Beta waves span a frequency of 13-22 Hz or even higher.
 The region where these meet the alpha range is termed sigma activity.
\end_layout

\begin_layout Subsection*
Gamma Waves
\end_layout

\begin_layout Standard
Any rhythms beyond the range of beta activity and have an amplitude less
 than 2 
\begin_inset Formula $\mu$
\end_inset

V are denoted as gamma waves, and are seen during intense mental activity
 and some kinds of sensory stimulation.
\begin_inset ERT
status open

\begin_layout Plain Layout


\backslash

\backslash

\end_layout

\end_inset


\end_layout

\begin_layout Standard
In addition to these fundamental wave types, some EEG events are of significance
 in the classification of sleep stages.
 S2 is particularly characterised by events known as sleep spindles and
 K-complexes.
 Sleep spindles are bursts of 12-16 Hz activity.
 A K-complex is a sharp negative wave that is followed by a slower positive
 wave, and occur randomly throughout S2 
\begin_inset CommandInset citation
LatexCommand cite
key "Susmakova2008"

\end_inset

.
 These events should last at least 0.5 seconds, and if they occur successively
 in a short enough time period the epoch is classified as S2.
\end_layout

\begin_layout Standard
\begin_inset Float table
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Tabular
<lyxtabular version="3" rows="7" columns="2">
<features tabularvalignment="middle">
<column alignment="center" valignment="top" width="0">
<column alignment="center" valignment="top" width="80col%">
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Stage
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Criteria
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Wake
\end_layout

\end_inset
</cell>
<cell multicolumn="1" alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none" width="85text%">
\begin_inset Text

\begin_layout Plain Layout
>50% of the page (epoch) consists of alpha (8-13 Hz) activity or low voltage,
 mixed (2-7 Hz) frequency activity.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
S1
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
50% of the epoch consists of relatively low voltage mixed (2-7 Hz) activity,
 and <50% of the epoch contains alpha activity.
 Slow rolling eye movements lasting several seconds often seen in early
 stage 1.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
S2
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Appearance of sleep spindles and/or K complexes and <20% of the epoch may
 contain high voltage (>75 μV, <2 Hz) activity.
 Sleep spindles and K complexes each must last >0.5 seconds.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
S3
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
20%-50% of the epoch consists of high voltage (>75 μV), low frequency (<2
 Hz) activity.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
S4
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
>50% of the epoch consists of high voltage (>75 μV) <2 Hz delta activity.
\end_layout

\end_inset
</cell>
</row>
<row>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
REM
\end_layout

\end_inset
</cell>
<cell alignment="center" valignment="top" topline="true" bottomline="true" leftline="true" rightline="true" usebox="none">
\begin_inset Text

\begin_layout Plain Layout
Relatively low voltage mixed (2-7 Hz) frequency EEG with episodic rapid
 eye movements and absent or reduced chin EMG activity.
\end_layout

\end_inset
</cell>
</row>
</lyxtabular>

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Rechtschaffen and Kales Sleep Staging Criteria 
\begin_inset CommandInset citation
LatexCommand cite
key "Spiegel19991435"

\end_inset

 
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Section
Feature Extraction
\end_layout

\begin_layout Standard
Automated classification systems generally rely upon a set of computed features
 that represent the primary attributes of each epoch.
 Significant effort has been put into producing effective methods of feature
 generation that precisely encapsulate the events particular to sleep recordings
, so that classification methods in turn will more accurately label new
 samples.
 Many approaches aim to extract the same factors used in evaluating the
 R&K criteria, while other attempts have been made to find non-linear measures
 in EEG data which are more useful for automated classification.
\end_layout

\begin_layout Subsection
Frequency Analysis
\end_layout

\begin_layout Standard
Since sleep recordings are described in terms of waveforms, signal processing
 techniques can be applied to sleep data to condense the input into a manageable
 feature vector.
 Though there is no one method used to gather spectral features, there are
 several studies that share common processes 
\begin_inset CommandInset citation
LatexCommand cite
key "pan2012transition,6121664,5138842,Zoubek2007171,Guenes2010"

\end_inset

.
 Most of these are focused upon the EEG channels in particular, for the
 purposes of separating the different wave features present in the sample.
\end_layout

\begin_layout Standard
Firstly, the sample is filtered using Butterworth bandpass filters of the
 
\begin_inset Formula $6^{th}$
\end_inset

 to 
\begin_inset Formula $8^{th}$
\end_inset

 order.
 Several of these are used, usually filtering bands that correspond to the
 frequency ranges of alpha, beta, gamma, delta and theta wave types, or
 the entire range of 0.5-30 Hz.
 Fast Fourier Transforms (FFTs) are then commonly used to transform the
 data from the time domain into the frequency domain.
 In this domain the total spectral energy for different frequency bands
 can be evaluated for an epoch by summing over that range in the frequency
 domain.
\end_layout

\begin_layout Standard
These spectral energy values can then be used as features or further combined
 to create a more complex feature set.
 Processing steps to measure the presence of sleep spindles, as well as
 calculating ratios of spectral powers was used by Pan et al.
 to extend feature vectors into thirteen dimensions 
\begin_inset CommandInset citation
LatexCommand cite
key "pan2012transition"

\end_inset

.
 Vector quantization was then performed upon these vectors as a measure
 to reduce computational complexity when performing classification.
 This was done by using an unsupervised training process to create a codebook
 of observation codes.
\end_layout

\begin_layout Standard
Alternate signal processing mechanisms have been used instead of or in conjuncti
on with the ones described.
 With the goal of detecting interference artifacts in EEG data, Roháľová
 et al.
 employ a Kalman Filter which acts in estimating the state of the system
 from measurements which contain random errors 
\begin_inset CommandInset citation
LatexCommand cite
key "rohalova2001detection"

\end_inset

.
 In the Kalman Filter they then embed an autoregressive model, a linear
 predictor used for modeling stationary time series.
 This conjunction with the Kalman Filter allows the model to be used for
 non-stationary signals such as EEG.
 However, this is shown to be not as effective as a different model which
 uses neural networks and an Extended Kalman Filter 
\begin_inset CommandInset citation
LatexCommand cite
key "rohalova2001detection"

\end_inset

.
 Use of a Hilbert-Huang Transform has been shown to improve the classification
 accuracy for particular sleep stages than the Fourier transform described
 previously 
\begin_inset CommandInset citation
LatexCommand cite
key "5138842"

\end_inset

, and the feature set made by this process was used to classify with an
 average accuracy rate of 81.7% with a nearest neighbor method.
 Wavelet packet transforms have also been applied onto EEG signals for classific
ation 
\begin_inset CommandInset citation
LatexCommand cite
key "4649365,Sinha2008"

\end_inset

.
 Wavelet transforms are a tool that promise improved accuracy in locating
 transient (high frequency) waves while maintaining a good frequency resolution
 for slow waves 
\begin_inset CommandInset citation
LatexCommand cite
key "4649365"

\end_inset

.
 While this seems ideal for EEG readings, being a dynamic signal that is
 prone to spikes and usually corrupted with noise, Wavelet transforms have
 been shown to be no better than Fourier transforms when the two have been
 compared using the same data set 
\begin_inset CommandInset citation
LatexCommand cite
key "Zoubek2007171"

\end_inset

, and since they take longer to process than FFT there seems little motivation
 to use Wavelet transform features.
 
\end_layout

\begin_layout Standard
Also of importance is the selection of which variables should be used for
 automatic classification.
 An over abundance of features can impact negatively upon machine learning
 processes, as unimportant variables can reduce the accuracy of classifiers
 and unnecessarily increase the computational complexity.
 A mutual information measure is used by Herrera et al.
\begin_inset CommandInset citation
LatexCommand cite
key "6121664"

\end_inset

 to identify non-linear relations between the variables.
 The mutual information estimator is designed in such a way to maximise
 the relevance of features while minimising redundancy between them.
 The algorithm proposed incrementally ranks all the potential variables,
 and then selects a subset of the top variables which maximises the classificati
on performance.
 While an analysis of the results shows that redundant variables were indeed
 eliminated, the classification done in the study did not surpass the performanc
e of those done in other works.
 However it was noted that the choice of classifier, as well as the fact
 that only a single channel was used for classification, could account for
 the level of accuracy demonstrated and thus the variable elimination procedure
 itself might still be useful.
 Similarly, an incremental selection of features was performed by Zoubek
 et al.
 
\begin_inset CommandInset citation
LatexCommand cite
key "Zoubek2007171"

\end_inset

 using an accuracy measure as criteria for selection.
 Various classifiers were used to ensure that the processing itself is insensiti
ve to what classifier is used, and overall the selection of features improved
 the classification of the sleep/wake stages.
\end_layout

\begin_layout Subsection
Non-Linear Feature Extraction
\end_layout

\begin_layout Standard
While using linear spectral features remains popular, there are arguments
 for the adoption of non-linear measures in the classification of EEG signals.
 There are many facts that indicate that the dynamics of the brain are non-linea
r in nature, and that many feedback loops exist at the neuronal level 
\begin_inset CommandInset citation
LatexCommand cite
key "raey"

\end_inset

.
 As described by Fell et al., there are two branches of thought on non-linear
 EEG analysis that have emerged: an 'ontological branch' and a 'pragmatic
 branch' 
\begin_inset CommandInset citation
LatexCommand cite
key "Fell1996401"

\end_inset

.
 The ontological branch questions whether the data from EEG readings can
 indeed be sufficiently explained in non-linear terms as opposed to linear
 stochastic models.
 Though there is evidence that may suggest this is the case it still remains
 an open question, and thus there is no certainty as to whether non-linear
 features best describe the data being analysed.
\end_layout

\begin_layout Standard
The second branch of thought described, the pragmatic branch, instead disregards
 the questions as to whether the data fits the dynamical models in reality
 and asks whether these models can reveal useful information that cannot
 be attained by linear spectral measures alone and which can be useful in
 EEG analysis.
 The study then proceeds to compare the effectiveness of linear vs non-linear
 measures in classifying the R&K stages, to examine whether this is the
 case.
\end_layout

\begin_layout Standard
Linear features, including those calculated from spectral analysis as well
 as some from the time domain were chosen in the study.
 These were contrasted with the non-linear measures of the largest Lyapunov
 exponent, approximated Kolmogorof entropy and correlation dimension.
 It was concluded that the non-linear measures added non-redundant information,
 and aided the discrimination between S1 and S2.
 However, spectral measures hold advantages in distinguishing S2 from Slow
 Wave Sleep (R&K stages S3 and S4 combined).
 Thus, a combination of both linear and non-linear features is recommended
 over spectral measures alone.
\end_layout

\begin_layout Standard
Non-linear features cannot be used for effective classification of all sleep
 stages however.
 Attempts at classifying REM sleep using largest Lyapunov exponent and correlati
on dimension on neural networks by Grözinger et al.
 
\begin_inset CommandInset citation
LatexCommand cite
key "raey"

\end_inset

 provide no improvement when combined with spectral power features.
 Indeed, when used alone the non-linear features performed significantly
 worse in classification of the used data set.
 It is then clear that the choice of features used is highly dependent upon
 the experimental situations and the kinds of EEG data used.
\end_layout

\begin_layout Section
Classification Methods
\end_layout

\begin_layout Subsection
Neural Networks
\end_layout

\begin_layout Standard
Artificial neural networks are a common choice of classifier for identification
 of sleep stages, and have been used with a variety of feature sets 
\begin_inset CommandInset citation
LatexCommand cite
key "4649365,Schaltenbrand1993157,Principe1993399,Sinha2008,raey,Becq2005"

\end_inset

.
 A description of a neural network system used for sleep stage classification
 is given by Schaltenbrand et al.
 
\begin_inset CommandInset citation
LatexCommand cite
key "Schaltenbrand1993157"

\end_inset

.
 The model used was a multilayer perceptron.
 Since single layer perceptrons can only solve linearly separable problems,
 a hidden layer of perceptrons is necessary to create a non-linear separation
 boundary suitable for such a complex data set as polysomnography readings.
 Each feature of the input vector is connected to a neuron on the input
 layer.
 These are all fully connected to neurons in the hidden layer, which are
 themselves fully connected to an output layer, which has a neuron for each
 of the six R&K stages.
 Each of these connections has an associated weight which scales the values
 given as activation input for each perceptron.
 Each neuron uses an activation function (typically a sigmoid function)
 which maps the sum of all inputs to an output value.
 These weights are trained using a back-propagation algorithm, which is
 effectively a gradient search function minimising the error between the
 desired and actual values on the output layer.
\end_layout

\begin_layout Standard
Configurations of neural networks differ between implementations.
 For example, the network described by Schaltenbrand et al.
 
\begin_inset CommandInset citation
LatexCommand cite
key "Schaltenbrand1993157"

\end_inset

 uses 10 units in the hidden layer for 17 inputs, while another 
\begin_inset CommandInset citation
LatexCommand cite
key "4649365"

\end_inset

 uses 8 hidden neurons for 12 inputs.
 Since there is no method to guarantee which amount of hidden neurons will
 optimise the classification, the configuration is discovered on a contextual
 basis.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout
\begin_inset Graphics
	filename MLP.png
	width 70col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
A multi-layer neural network 
\begin_inset CommandInset citation
LatexCommand cite
key "Schaltenbrand1993157"

\end_inset


\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

\begin_layout Subsection
Comparisons With Other Methods
\end_layout

\begin_layout Standard
A comparison between the neural networks described above and four other
 traditional classifiers was conducted to evaluate performance on classifying
 sleep stages 
\begin_inset CommandInset citation
LatexCommand cite
key "Becq2005"

\end_inset

.
 Eight features were created from both the time and frequency domain, and
 were used for classification with linear and quadratic classifiers, k nearest
 neighbors, Parzen kernels and a neural network classifier.
 While none of the methods studied achieved the level of accuracy that is
 seen in human scorers (who typically disagree 10-20% of the time), they
 are close to the interval.
 Neural networks were identified as advantageous in that additional data
 transformations is not necessary to improve results, they can deal with
 non-Gaussian probability distributions and can handle extreme values.
 Additionally they are not as resource consuming as k-nearest neighbor classifie
rs, which although achieving the best results with homogeneous data require
 storing large amounts of training vectors in memory.
\end_layout

\begin_layout Standard
A separate comparison between a neural network, belief automation and expert
 system 
\begin_inset CommandInset citation
LatexCommand cite
key "Principe1993399"

\end_inset

 supports the idea that automated systems do not yet reach a satisfactory
 level of agreement with human scorers, but suggests that the similarity
 in results between the different classification systems indicate that there
 is a need for enhancing the accuracy of the initial EEG feature extraction
 as opposed to the need for a better classifier.
\end_layout

\begin_layout Subsection
Hidden Markov Models
\end_layout

\begin_layout Standard
Also gaining popularity is the use of Hidden Markov Models (HMMs) to identify
 the sequence of sleep stages.
 Described in the classical text by Rabiner and Juang 
\begin_inset CommandInset citation
LatexCommand cite
key "1165342"

\end_inset

, a HMM model contains a stochastic observable process and an underlying
 non-observable stochastic process.
 This hidden process is a sequence of states from the finite set 
\begin_inset Formula $Q=\{q_{1},q_{2},...,q_{n}\}$
\end_inset

.
 Denoted as a triplet 
\begin_inset Formula $\lambda=(\pi,A,B)$
\end_inset

, where 
\begin_inset Formula $\pi$
\end_inset

 is the prior probability distribution of hidden states, 
\begin_inset Formula $A=\{a_{ij}\}$
\end_inset

 is the transition probability distribution between hidden states and B
 is the observation distribution, a HMM describes a temporal process where
 the current state at time 
\begin_inset Formula $t$
\end_inset

 is conditionally dependent upon the state at time 
\begin_inset Formula $t-1$
\end_inset

 and the current observation value.
 Such a model can be used to answer three problems:
\end_layout

\begin_layout Enumerate
Given an observation sequence 
\begin_inset Formula $O=O_{1},O_{2},...,O_{T}$
\end_inset

, and the model 
\begin_inset Formula $\lambda$
\end_inset

, compute the probability of the observation sequence 
\begin_inset Formula $P(O|\lambda)$
\end_inset

.
\end_layout

\begin_layout Enumerate
Given an observation sequence 
\begin_inset Formula $O=O_{1},O_{2},...,O_{T}$
\end_inset

, and the model 
\begin_inset Formula $\lambda$
\end_inset

, determine a state sequence 
\begin_inset Formula $I=I_{1},I_{2},...,I_{T}$
\end_inset

 which is optimal in a meaningful sense.
\end_layout

\begin_layout Enumerate
Given an observation sequence 
\begin_inset Formula $O=O_{1},O_{2},...,O_{t}$
\end_inset

, adjust a model 
\begin_inset Formula $\lambda$
\end_inset

 to maximise 
\begin_inset Formula $P(O|\lambda)$
\end_inset

.
\end_layout

\begin_layout Standard
\begin_inset Float figure
wide false
sideways false
status open

\begin_layout Plain Layout

\end_layout

\begin_layout Plain Layout
\begin_inset Graphics
	filename hmm.png
	width 75col%

\end_inset


\begin_inset Caption

\begin_layout Plain Layout
Trellis diagram representing a basic HMM.
 Empty circles are hidden states and shaded ones are observation nodes 
\begin_inset CommandInset citation
LatexCommand cite
key "zhong2002hmms"

\end_inset

.
 
\end_layout

\end_inset


\end_layout

\begin_layout Plain Layout

\end_layout

\end_inset


\end_layout

\begin_layout Standard
For the task of sleep staging we are interested in solving problem 2, and
 Hidden Markov Models appear to be the perfect model for EEG analysis since
 the information is non-stationary and non-localised 
\begin_inset CommandInset citation
LatexCommand cite
key "Penny1998"

\end_inset

.
 The optimality criteria referred to in this problem is to find the single
 best state sequence with the highest probability, and there exists a formal
 procedure for finding this called the Viterbi algorithm 
\begin_inset CommandInset citation
LatexCommand cite
key "1165342"

\end_inset

.
 For sleep staging, the hidden states in the HMM are the R&K labels, and
 the observations are the feature vectors which can be created by any of
 the means previously described.
 Experiments using HMM classifiers have proven to provide results in good
 agreement with experts 
\begin_inset CommandInset citation
LatexCommand cite
key "Doroshenkov2007"

\end_inset

.
\end_layout

\begin_layout Standard
A range of variations of the classic Hidden Markov Model have been used
 for the task of classifying sleep stages.
 Pan et al.
 introduce transition constraints upon the model, so that transitions between
 sleep stages that are known to be impossible or improbable are not chosen.
 This has significantly enhanced the recognition rate when compared to prior
 studies 
\begin_inset CommandInset citation
LatexCommand cite
key "pan2012transition"

\end_inset

.
 Several Coupled Hidden Markov Models have been proposed by Zhong and Ghosh,
 which are designed to better characterise multiple interdependent sequences
 
\begin_inset CommandInset citation
LatexCommand cite
key "zhong2002hmms"

\end_inset

.
 However, while multivariate HMMs have good classification accuracy, using
 a more complex CHMM does not necessarily create better results, due to
 an increased amount of complexity and associated assumptions.
\end_layout

\begin_layout Standard
Other attempts have used Gaussian Hidden Markov Models, which use Gaussian
 observation densities instead of discrete ones 
\begin_inset CommandInset citation
LatexCommand cite
key "Penny1998,doi:10.1080/088395102753559271,Flexer2005199"

\end_inset

.
 The simplest of these uses a distribution of 
\begin_inset Formula $B=b_{j}(x),b_{j}(x)=\mathcal{N}[x,\mu,U_{j}]$
\end_inset

, where 
\begin_inset Formula $\mu_{j}$
\end_inset

 and 
\begin_inset Formula $U_{j}$
\end_inset

 are the mean vector and covariance matrix associated with state 
\begin_inset Formula $j$
\end_inset

 
\begin_inset CommandInset citation
LatexCommand cite
key "Flexer2005199"

\end_inset

.
 These were then used to create a sleep scoring system on probabilistic
 principles as opposed to the traditional R&K rules, and thus the performance
 of these cannot be directly compared to previous results 
\begin_inset CommandInset citation
LatexCommand cite
key "doi:10.1080/088395102753559271,Flexer2005199"

\end_inset

.
\end_layout

\begin_layout Section
Concerns With R&K Scoring
\end_layout

\begin_layout Standard
Through the Rechtschaffen and Kales guidelines are the prevailing standard,
 there is increasing dissatisfaction within the sleep research community
 as to the validity of R&K sleep staging
\begin_inset CommandInset citation
LatexCommand cite
key "Flexer2005199"

\end_inset

.
 Firstly, it is a very tedious task with room for subjective interpretation
 in the visual identification of stages.
 Secondly, the standard is defined in terms of a low 30 second temporal
 resolution, and accounts for only 6 stages neglecting any smaller structures
 of sleep 
\begin_inset CommandInset citation
LatexCommand cite
key "doi:10.1080/088395102753559271"

\end_inset

.
 This causes epochs that lie on the transition of two sleep phases to be
 more likely to be misclassified.
 The manual also only calls for a single optimal central EEG derivation
 for scoring sleep, and does not take into account regional differences
 as long as certain types of activity are registered 
\begin_inset CommandInset citation
LatexCommand cite
key "silber2007visual"

\end_inset

.
 It is thus questionable, in an age where over 16 channels can be simultaneously
 recorded with ease, whether a single derivation is an adequate measure.
\end_layout

\begin_layout Standard
There is also concern with the level of agreement between experts.
 Since sleep staging is a subjective process done via visual analysis, disagreem
ents between experts is common, occurring approximately 20% of the time
 
\begin_inset CommandInset citation
LatexCommand cite
key "JSR:JSR700"

\end_inset

.
 Furthermore, the level of disagreement is even higher between different
 sleep laboratories, which makes analysis between labs difficult 
\begin_inset CommandInset citation
LatexCommand cite
key "PMID:11083599"

\end_inset

.
 These issues carry over onto automatic classification.
 Inconsistencies between data sets make it difficult to create a classifier
 that can be used universally, and training data has to be created for each
 lab 
\begin_inset CommandInset citation
LatexCommand cite
key "Flexer2005199"

\end_inset

.
 Alternatives such as the AASM standard have been proposed to replace the
 R&K rules, which have improved the agreement between scorers for all stages
 but one 
\begin_inset CommandInset citation
LatexCommand cite
key "PMID:11083599"

\end_inset

.
\end_layout

\begin_layout Standard

\end_layout

\begin_layout Section
Summary
\end_layout

\begin_layout Standard
Distinguishing sleep phases from polysomnography readings is a non-trivial
 task with practical applications.
 While there have been numerous attempts to provide automated means of sleep
 staging under the R&K standard, there has yet been no single method that
 promises performance at an expert level.
 Though performance can be improved using classifiers optimised for the
 task, the correct choice of features proves to be more critical in affecting
 classification accuracy.
 It is worthwhile to examine further whether using additional data channels
 in conjunction with EEG derivations can provide useful information for
 classification, as well as an inquiry as to improving the way sleep phases
 themselves are defined.
\end_layout

\begin_layout Standard
\begin_inset CommandInset bibtex
LatexCommand bibtex
bibfiles "reference"
options "plain"

\end_inset


\end_layout

\end_body
\end_document
