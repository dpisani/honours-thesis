Presentation
------------

 - intro (2m)
   - what is sleep staging
   - how its done (PSG)
   - problem: costly, time consuming

 - current methods (6m)
   - feature extraction (2m)
   - classifiers (MLP) (3m)
   - SFS (1m)

 - our method (9)
   - fingerprint (5)
     - power spectra (1m)
     - comparisons (frechet, MSE) (2m)
       - show pairs (1m)
     - feature extraction (1m)
   - classifier (4)
     - kmeans (2m)
     - SCC (2m)

 - results (3m)

 - conclusions/future works (2m)
