function epoch = extract_epoch(D, epochIndex)
  samples = 30 * D.rate;
  epochStart = (epochIndex-1)*samples + 1;
  epochEnd = epochIndex*samples;
  data = D.data(epochStart:epochEnd, 1:end);
  epoch = struct('data', data, 'rate', D.rate, 'chlabels', {D.chlabels});
