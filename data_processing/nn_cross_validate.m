function [] = nn_cross_validate(data, report_filename, figures_filename_prefix, figures_url_prefix)

  test_targets = [];
  test_outputs = [];
  CVO = cvpartition(data.labels,'k',10);
  train_time = 0;
  classify_time = 0;
  for fold=1:CVO.NumTestSets,
    trIdx = CVO.training(fold);
    teIdx = CVO.test(fold);

    [outputs, labels, train_t, classify_t] = nn_process(data, trIdx, teIdx);
    train_time = train_time + train_t;
    classify_time = classify_time + classify_t;
    test_outputs = [test_outputs outputs];
    test_targets = [test_targets labels];
  end

  % Recalculate Training, Validation and Test Performance
  %  trainTargets = t .* tr.trainMask{1};
  % valTargets = t  .* tr.valMask{1};
  % testTargets = t  .* tr.testMask{1};
  % testOutputs = y .* tr.testMask{1};
  % ttind = vec2ind(testTargets);
  % tyind = vec2ind(testOutputs);
  %  trainPerformance = perform(net,trainTargets,y)
  % valPerformance = perform(net,valTargets,y);
  % testPerformance = perform(net,testTargets,y);
  tot_c_time = classify_time;
  tot_t_time = train_time;
  classify_time = classify_time / CVO.NumTestSets;
  train_time = train_time / CVO.NumTestSets;

  perfinfo = struct('targets', test_targets, 'outputs', test_outputs, 'train_time', train_time, 'classify_time', classify_time);

  if exist('figures_filename_prefix','var'),
    write_text_section(report_filename, 'About', '10 fold stratified cross validation was performed on all samples on the data set.');
    write_performance(perfinfo, report_filename, figures_filename_prefix, figures_url_prefix);
  else
    output_raw_perf(test_targets, test_outputs, tot_t_time, tot_c_time, report_filename);
  end

  % View the Network
  %view(net)

  % Uncomment these lines to enable various plots.
  %figure, plotperform(tr)
  %figure, plottrainstate(tr)

  % fig = figure, plotconfusion(t,y);
  % set(gca,'xticklabel',{'W' 'S1' 'S2' 'S3' 'S4' 'REM' 'All'});
  % set(gca,'yticklabel',{'W' 'S1' 'S2' 'S3' 'S4' 'REM' 'All'});
  % title('All Confusion');
  % %close(fig);
  % fig = figure, plotconfusion(testTargets,y);
  % set(gca,'xticklabel',{'W' 'S1' 'S2' 'S3' 'S4' 'REM' 'All'});
  % set(gca,'yticklabel',{'W' 'S1' 'S2' 'S3' 'S4' 'REM' 'All'});
  % title('Test Confusion');
  %close(fig);
  %figure, plotroc(t,y)
  %figure, ploterrhist(e)
