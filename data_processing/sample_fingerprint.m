function features = sample_fingerprint(D)
  %create an overall FFT for the entire signal

  %break up into 5s segments and combine the fft's
  time_seg = 5;
  segment_size = D.eeg.rate * time_seg; %5 seconds
  no_segments = floor(size(D.eeg.data,1) / segment_size);

  Pyy = [];
  for seg=1:no_segments,
    a = (seg-1)*segment_size + 1;
    b = (seg)*segment_size;
    data_seg = D.eeg.data(a:b, 1:end);
    spec = fft_process(data_seg, time_seg, D.eeg.rate);

    if size(Pyy,2) == 0
      Pyy = spec;
    else
      Pyy = Pyy + spec;
    end
  end

  spanD = band_span(D.eeg.rate, 0, 4, time_seg);
  spanTotal = band_span(D.eeg.rate, 0, 30, time_seg);
  spanSpindle = band_span(D.eeg.rate, 8, 16, time_seg);

  total_power = sum(Pyy(spanTotal(1):spanTotal(2), 1:end),1);
  d_power = sum(Pyy(spanD(1):spanD(2), 1:end),1) ./ total_power;
  spindle_power = sum(Pyy(spanSpindle(1):spanSpindle(2), 1:end),1) ./ total_power;

  %mean freq in the delta and spindle bands
  meanD = zeros(1, size(Pyy, 2));
  for b=spanD(1):spanD(2),
    freq = (b-1) * D.eeg.rate / (D.eeg.rate*time_seg);
    meanD = meanD + Pyy(b, 1:end) * freq;
  end
  meanD = meanD ./ d_power;

  meanSpindle = zeros(1, size(Pyy, 2));
  for b=spanSpindle(1):spanSpindle(2),
    freq = (b-1) * D.eeg.rate / (D.eeg.rate*time_seg);
    meanSpindle = meanSpindle + Pyy(b, 1:end) * freq;
  end
  meanSpindle = meanSpindle ./ d_power;

  SD = std(D.eeg.data);
  skew = skewness(D.eeg.data);

  %Mean Gradient
  GRAD_SP = [];
  GRAD_D = [];
  for ch=1:size(Pyy,2),
    g = gradient(Pyy(spanSpindle(1):spanSpindle(2), ch));
    GRAD_SP = [GRAD_SP mean(g)];

    g = gradient(Pyy(spanD(1):spanD(2), ch));
    GRAD_D = [GRAD_D mean(g)];
  end


  features = [d_power spindle_power meanD meanSpindle SD skew GRAD_SP GRAD_D];
