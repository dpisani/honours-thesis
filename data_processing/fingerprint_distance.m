distance = [];
for m=1:length(fingerprints),
  for n=1:length(fingerprints),
    A = fingerprints(m, :);
    B = fingerprints(n, :);
    d = A - B;
    distance(m,n) = sqrt(d * d');
  end
end

max_dist = max(max(distance));
min_dist = min(min(distance));

distance = (distance - min_dist) / (max_dist - min_dist);

hFig = figure(2);
set(hFig, 'Position', [0 0 700 700])
colormap('bone');

imagesc(distance);
title('Fingerprint Distance Between Nights');
xlabel('Night')
ylabel('Night')

max_dist = max(max(sim));
min_dist = min(min(sim));

sim =  (sim - min_dist) / (max_dist - min_dist);

simdiff = abs(sim-distance);




sdl = tril(simdiff);
sdl = sdl ( : );
sdl = sdl(sdl~=0);

avg = mean(sdl)
sd = std(sdl)
