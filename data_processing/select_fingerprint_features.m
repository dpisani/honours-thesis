function feature_idx = select_fingerprint_features(featureset)
  k = 3;
  %selection criteria
  function score = cluster_criteria(fingerprints)
    test_outputs = [];
    test_targets = [];
    for s=1:size(featureset.samples,2),
      cluster_idx = [1:s-1 s+1:size(featureset.samples,2)];
      %create clusters
      [c_idx, centroids] = kmeans(fingerprints(cluster_idx, :), k);

      %find closest centroid
      test_fingerprint = fingerprints(s, :);
      closest_c = 0;
      closest_distance = 0;
      for c=1:k,
        centroid = centroids(c, :);
        dis = pdist([centroid ; test_fingerprint]);
        if closest_c == 0 || closest_distance > dis,
          closest_distance = dis;
          closest_c = c;
        end
      end

      %get indices of this sample
      trainIdx = [];
      testIdx = featureset.idx{s};
      for ii = 1:size(cluster_idx,2),
        if c_idx(ii) == closest_c,
          trainIdx = [trainIdx featureset.idx{cluster_idx(ii)}];
        end
      end

      %run the NN
      [outputs, labels, train_time, classify_time] = nn_process(featureset.samples_combined, trainIdx, testIdx);
      test_outputs = [test_outputs outputs];
      test_targets = [test_targets labels];

    end
    CP = classperf(test_targets, test_outputs);
    score = sum(CP.ErrorDistributionByClass)
  end

  fun = @(XT)...
      (cluster_criteria(XT));

  inmodel = sequentialfs(fun, featureset.fingerprints, 'cv', 'none', 'direction', 'backward');

  feature_idx = inmodel;
end
