function [] = cluster_train_subjects(featureset, report_filename, figures_filename_prefix, figures_url_prefix)
bestAcc = 0;
bestPerf = NaN;
worstAcc = 1;
worstPerf = NaN;
outputs_combined = [];
targets_combined = [];
result_outputs = {};
result_targets = {};
train_times = {};
classify_times = {};
train_t = 0;
class_t = 0;
scores = [];
for s=1:size(featureset.samples,2),
  data = featureset.samples{s};
  test_targets = [];
  test_outputs = [];
  CVO = cvpartition(data.labels,'k',10);
  train_time = 0;
  classify_time = 0;
  for fold=1:CVO.NumTestSets,
    trIdx = CVO.training(fold);
    teIdx = CVO.test(fold);

    [outputs, labels, train_t, classify_t] = nn_process(data, trIdx, teIdx);
    train_time = train_time + train_t;
    classify_time = classify_time + classify_t;
    test_outputs = [test_outputs outputs];
    test_targets = [test_targets labels];
  end

  classify_time = classify_time / CVO.NumTestSets;
  train_time = train_time / CVO.NumTestSets;

  perfinfo = struct('targets', test_targets, 'outputs', test_outputs, 'train_time', train_time, 'classify_time', classify_time);

  outputs_combined = [outputs_combined test_outputs];
  targets_combined = [targets_combined test_targets];
  result_outputs{s} = test_outputs;
  result_targets{s} = test_targets;
  train_times{s} = train_time;
  classify_times{s} = classify_time;
  train_t = train_t + train_time;
  class_t = class_t + classify_time;

  CP = classperf(perfinfo.targets, perfinfo.outputs);

  scores = [scores CP.CorrectRate];

  if CP.CorrectRate > bestAcc,
    bestAcc = CP.CorrectRate;
    bestPerf = perfinfo;
  end
  if CP.CorrectRate < worstAcc,
    worstAcc = CP.CorrectRate;
    worstPerf = perfinfo;
  end
end

if exist('figures_filename_prefix','var'),
  write_text_section(report_filename, 'About', '10 fold startified cross validation was done on a per-subject basis.');

  allPerf = struct('targets', targets_combined, 'outputs', outputs_combined, 'train_time', train_t, 'classify_time', class_t);

  write_group_perf(bestPerf, worstPerf, allPerf, scores, report_filename, figures_filename_prefix, figures_url_prefix);
else
  output_raw_perf(targets_combined, outputs_combined, train_t, class_t, report_filename);
  output_individual_raw_perf(result_targets, result_outputs, train_times, classify_times, report_filename);
end
