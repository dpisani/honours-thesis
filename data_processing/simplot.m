sim = [];
for m=1:length(similarity),
row = similarity{m};
for n=1:length(row),
cell = row{n};
sim(m,n) = mean(cell.frechet);
end
end
hFig = figure(1);
set(hFig, 'Position', [0 0 700 700])
colormap('bone');

imagesc(sim);
title('Sigma Band Frechet Distance Between Nights');
xlabel('Night')
ylabel('Night')
