function span = band_span(sample_frequency, freq_min, freq_max, duration)
	number_of_points = duration * sample_frequency;
	df = sample_frequency / number_of_points;
	a = floor(freq_min / df)+1;
	b = floor(freq_max/ df);
	span = [a, b];
end
