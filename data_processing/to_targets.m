function targets = to_targets( epoch_scores )
  %TO_TARGETS Summary of this function goes here
  %   Detailed explanation goes here
  targets = [];
  for i=1:size(epoch_scores)-1,
    target = -1;
    x = epoch_scores{i};
    if x == 'W'
      target = [1,0,0,0,0,0];
    elseif x == '1'
      target = [0,1,0,0,0,0];
    elseif x == '2'
      target = [0,0,1,0,0,0];
    elseif x == '3'
      target = [0,0,0,1,0,0];
    elseif x == '4'
      target = [0,0,0,0,1,0];
    elseif x == 'R'
      target = [0,0,0,0,0,1];
    end
    targets = [targets; target];
  end
end
