function kappa = calculate_kappa(targets, outputs)
k_table = zeros(6,6);
for ex=1:size(targets, 2),
  a = targets(ex);
  b = outputs(ex);
  k_table(a,b) = k_table(a,b) + 1;
end
k_table = k_table ./ size(targets, 2);

Po = 0;
for ind=1:6,
  Po = Po + k_table(ind,ind);
end

Pe = 0;
for ind=1:6,
  col_v = 0;
  for col=1:6,
    col_v = col_v + k_table(ind, col);
  end
  row_v = 0;
  for row=1:6,
    row_v = row_v + k_table(row, ind);
  end

  Pe = Pe + (col_v * row_v);
end

kappa = (Po - Pe) / (1 - Pe);
