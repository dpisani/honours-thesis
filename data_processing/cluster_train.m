%inputs
%data - data set
%k: number of clusters to make
%#function network
function [] = cluster_train(featureset, k, report_filename, figures_filename_prefix, figures_url_prefix)
  bestAcc = 0;
  bestPerf = NaN;
  worstAcc = 1;
  worstPerf = NaN;
  outputs_combined = [];
  targets_combined = [];
  result_outputs = {};
  result_targets = {};
  train_times = {};
  classify_times = {};
  train_t = 0;
  class_t = 0;
  scores = [];
  for s=1:size(featureset.samples,2),
    cluster_idx = [1:s-1 s+1:size(featureset.samples,2)];
    %create clusters
    [c_idx, centroids] = kmeans(featureset.fingerprints(cluster_idx, :), k);

    %find closest centroid
    test_fingerprint = featureset.fingerprints(s, :);
    closest_c = 0;
    closest_distance = 0;
    for c=1:k,
      centroid = centroids(c, :);
      dis = pdist([centroid ; test_fingerprint]);
      if closest_c == 0 || closest_distance > dis,
        closest_distance = dis;
        closest_c = c;
      end
    end

    %get indices of this sample
    trainIdx = [];
    testIdx = featureset.idx{s};
    for ii = 1:size(cluster_idx,2),
      if c_idx(ii) == closest_c,
        trainIdx = [trainIdx featureset.idx{cluster_idx(ii)}];
      end
    end

    %run the NN
    [test_outputs, test_labels, train_time, classify_time] = nn_process(featureset.samples_combined, trainIdx, testIdx);
    perfinfo = struct('targets', test_labels, 'outputs', test_outputs, 'train_time', train_time, 'classify_time', classify_time);
    outputs_combined = [outputs_combined test_outputs];
    targets_combined = [targets_combined test_labels];
    result_outputs{s} = test_outputs;
    result_targets{s} = test_labels;
    train_times{s} = train_time;
    classify_times{s} = classify_time;
    train_t = train_t + train_time;
    class_t = class_t + classify_time;

    CP = classperf(perfinfo.targets, perfinfo.outputs);

    scores = [scores CP.CorrectRate];

    if CP.CorrectRate > bestAcc,
      bestAcc = CP.CorrectRate;
      bestPerf = perfinfo;
    end
    if CP.CorrectRate < worstAcc,
      worstAcc = CP.CorrectRate;
      worstPerf = perfinfo;
    end
  end

  if exist('figures_filename_prefix','var'),
    write_text_section(report_filename, 'About', 'Only one night was used as testing. The remaining were clustered. The test sample was classified under the closest cluster.');

    allPerf = struct('targets', targets_combined, 'outputs', outputs_combined, 'train_time', train_t / size(featureset.samples,2), 'classify_time', class_t / size(featureset.samples,2));

    write_group_perf(bestPerf, worstPerf, allPerf, scores, report_filename, figures_filename_prefix, figures_url_prefix);
  else
    output_raw_perf(targets_combined, outputs_combined, train_t, class_t, report_filename);
    output_individual_raw_perf(result_targets, result_outputs, train_times, classify_times, report_filename);
  end
