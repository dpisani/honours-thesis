% transform time domain to frequency domain

%INPUTS
%epoch: matrix of sampled signal
%duration: length (seconds) of the signal
%sampling_rate: samples/second of the input epoch

function spectrum = fft_process(epoch, duration, sampling_rate)
  samples = duration * sampling_rate;
  epoch = epoch(1:samples, 1:end);
  NFFT = 2^nextpow2(samples);
  epoch(NFFT, end) = 0;
  spectrum = fft(epoch);
  indexNyquist = NFFT/2+1;
  %vicinity of Nyquist, which is in the middle
  spectrum = spectrum(1:indexNyquist, 1:end); %truncate
  spectrum = spectrum/NFFT; %scale
  spectrum(2:end, 1:end) = 2 * spectrum(2:end, 1:end); %compensate for truncating the negative
  spectrum = abs(spectrum);
end
