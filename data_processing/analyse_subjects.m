test_prefix = 'SE_k1';

targets = {};
outputs = {};
accuracy = [];
for t=1:100,
  fname = strcat('results/', test_prefix, '_n', num2str(t), '_results_indiv.mat');
  results = load(fname);

  for s=1:length(results.data),
    if t == 1,
      targets{s} = results.data{s}.targets;
      outputs{s} = results.data{s}.outputs;
    else
      targets{s} = [targets{s} results.data{s}.targets];
      outputs{s} = [outputs{s} results.data{s}.outputs];
    end

    scp = classperf(results.data{s}.targets, results.data{s}.outputs);
    accuracy = [accuracy scp.CorrectRate];

  end

end

CP = {};
for s=1:length(targets),
  CP{s} = classperf(targets{s}, outputs{s});
end
