function feature_idx = select_epoch_features(featureset)

  function score = classify_criteria(X, y)
    test_targets = [];
    test_outputs = [];
    CVO = cvpartition(featureset.samples_combined.labels,'k',10);
    for fold=1:CVO.NumTestSets,
      trIdx = CVO.training(fold);
      teIdx = CVO.test(fold);
      data = struct('features', X, 'targets', y);
      [outputs, labels, train_t, classify_t] = nn_process(data, trIdx, teIdx);
      test_outputs = [test_outputs outputs];
      test_targets = [test_targets labels];
    end
    CP = classperf(test_targets, test_outputs);
    score = sum(CP.ErrorDistributionByClass);
  end

  fun = @(XT,yT)...
      (classify_criteria(XT,yT));

  %keepout = [33 34 35 36];
  %keepout = [29 30 31 32];
  inmodel = sequentialfs(fun, featureset.samples_combined.features, featureset.samples_combined.targets, 'cv', 'none');

  feature_idx = inmodel;
end
