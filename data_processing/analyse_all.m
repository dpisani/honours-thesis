test_prefix = 'SE_k1';

targets = [];
outputs = [];
train_time = 0;
classify_time = 0;
for t=1:100,
  fname = strcat('results/', test_prefix, '_n', num2str(t), '_results_all.mat');
  results = load(fname);

  targets = [targets results.data.targets];
  outputs = [outputs results.data.outputs];
  train_time = train_time + results.data.train_time;
  classify_time = classify_time + results.data.classify_time;
end

train_time = train_time / 10
classify_time = classify_time / 10
CP = classperf(targets, outputs)
