% Solve a Pattern Recognition Problem with a Neural Network

function [test_outputs, test_labels, train_time, classify_time] = nn_process(data, train_val_index, test_index)
  ix = data.features';
  it = data.targets';

  x = ix(:, train_val_index);
  t = it(:, train_val_index);

  [net, train_time] = train_nn(x, t);
  % Test the Network
  trx = ix(:, test_index);
  trt = it(:, test_index);

  %measure classification time
  tic
  y = net(trx);

  classify_time = toc;

  test_labels = vec2ind(trt);
  test_outputs = vec2ind(y);
