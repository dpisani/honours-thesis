function corel = cluster_correlation(similarity, c_idx, k)
  corel = [];
  for c=1:length(c_idx),
    c_num = c_idx(c);
    for d=c+1:length(c_idx),
      d_num = c_idx(d);
      if c_num == d_num,
        if c_num == k,
          corel = [corel mean(similarity{c}{d}.mserr)];
        end
      end
    end
  end
