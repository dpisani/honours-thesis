%Runs all the tests and generates a report
function [] = run_tests(report_filename, figures_filename_prefix, figures_url_prefix)

  function [] = write_heading(heading)
    fid = fopen(report_filename,'a');
    if fid ~= -1
      fprintf(fid, ' "%s":\n', heading);
      fclose(fid);                     %# Close the file
    end
  end


  try
    %Load in the data and do feature processing straight up
    disp('Loading Data Sets');
    datasets = load_data();
    disp('Processing Data');
    featuresets = process_features();

    disp('Running Tests');

    %Fingerprint plots
    test_postfix = 'fingerprint_eval_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Fingerprints: Control Apnea');
    plot_fingerprints(datasets.control_apnea, featuresets.control_apnea.fingerprints, report_filename, fig_fname, fig_url);

    test_postfix = 'fingerprint_eval_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Fingerprints: Sleep-edfx');
    plot_fingerprints(datasets.sleep_edfx, featuresets.sleep_edfx.fingerprints, report_filename, fig_fname, fig_url);

    test_postfix = 'fingerprint_eval_d_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Delta Fingerprints: Control Apnea');
    plot_delta_fingerprints(datasets.control_apnea, featuresets.control_apnea.fingerprints, report_filename, fig_fname, fig_url);

    test_postfix = 'fingerprint_eval_d_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Delta Fingerprints: Sleep-edfx');
    plot_delta_fingerprints(datasets.sleep_edfx, featuresets.sleep_edfx.fingerprints, report_filename, fig_fname, fig_url);

    %Clustering tests
    test_postfix = 'clust_eval_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Clustering Evaluation: Control Apnea');
    evaluate_clustering(featuresets.control_apnea.fingerprints, report_filename, fig_fname, fig_url);

    test_postfix = 'clust_eval_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Clustering Evaluation: Sleep-edfx');
    evaluate_clustering(featuresets.sleep_edfx.fingerprints, report_filename, fig_fname, fig_url);

    test_postfix = 'clust_eval_se_n1';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Clustering Evaluation: Sleep-edfx (Night 1)');
    evaluate_clustering(featuresets.sleep_edfx.fingerprints(1:2:end, 1:end), report_filename, fig_fname, fig_url);

    test_postfix = 'clust_eval_se_n2';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Clustering Evaluation: Sleep-edfx (Night 2)');
    evaluate_clustering(featuresets.sleep_edfx.fingerprints(2:2:end, 1:end), report_filename, fig_fname, fig_url);

    %Run clustered training tests
    test_postfix = 'NN_clust_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Cluster Others: Control Apnea');
    cluster_train(featuresets.control_apnea, 4, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_clust_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Cluster Others: Sleep-edfx');
    cluster_train(featuresets.sleep_edfx, 4, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_clust_sen';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Cluster Others: Sleep-edfx (Across nights)');
    cluster_train_subjects(featuresets.sleep_edfx, 4, report_filename, fig_fname, fig_url);

    %Run randomised clustered training tests
    test_postfix = 'NN_clust_r_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Randomly Cluster Others: Control Apnea');
    random_cluster_train(featuresets.control_apnea, 4, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_clust_r_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Randomly Cluster Others: Sleep-edfx');
    random_cluster_train(featuresets.sleep_edfx, 4, report_filename, fig_fname, fig_url);

    %Run subject dependant tests
    test_postfix = 'NN_sub_dep_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Subject Dependant: Control Apnea');
    subject_dependant_classifier(featuresets.control_apnea, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_sub_dep_ses';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Subject Dependant: Sleep-edfx');
    subject_dependant_classifier(featuresets.sleep_edfx_subjects, report_filename, fig_fname, fig_url);

    %Run NN tests
    test_postfix = 'NN_o_v_a_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Test Others: Control Apnea');
    nn_one_v_others(featuresets.control_apnea, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_o_v_a_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Test Others: Sleep-edfx (per sample)');
    nn_one_v_others(featuresets.sleep_edfx, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_o_v_a_ses';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Train One Test Others: Sleep-edfx (per subject)');
    nn_one_v_others(featuresets.sleep_edfx_subjects, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_a_v_o_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Test One Train Others: Control Apnea');
    nn_others_v_one(featuresets.control_apnea, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_a_v_o_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Test One Train Others: Sleep-edfx (per sample)');
    nn_others_v_one(featuresets.sleep_edfx, report_filename, fig_fname, fig_url);

    test_postfix = 'NN_a_v_o_ses';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Test One Train Others: Sleep-edfx (per subject)');
    nn_others_v_one(featuresets.sleep_edfx_subjects, report_filename, fig_fname, fig_url);

    test_postfix = 'NNC_ca';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Control Apnea Combined');
    nn_cross_validate(featuresets.control_apnea.samples_combined, report_filename, fig_fname, fig_url);

    test_postfix = 'NNC_se';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Sleep-edfx Combined');
    nn_cross_validate(featuresets.sleep_edfx.samples_combined, report_filename, fig_fname, fig_url);

    test_postfix = 'NNC_car';
    fig_fname = strcat(figures_filename_prefix, test_postfix);
    fig_url = strcat(figures_url_prefix, test_postfix);
    write_heading('Control Apnea (reduced) Combined');
    write_text_section(report_filename, 'About', 'The control apnea dataset was processed to make it similar to sleep-edfx recordings. This uses Fz-Cz and Pz-Oz as opposed to the 6 channels in the original set. Note that the sleep-edfx set uses FPz-Cz instead of Fz-Cz');
    nn_cross_validate(featuresets.control_apnea_reduced.samples_combined, report_filename, fig_fname, fig_url);

    exit(0);
  catch ME
    fid = fopen(report_filename,'a');
    if fid ~= -1
      fprintf(fid, '\n---\n  "Error running tests:"\n  - "%s"\n  - "%s"\n  - "%s"\n', ME.message, ME.stack.file, ME.stack.line);
      fclose(fid);                     %# Close the file
    end
    exit(1);
  end


end
