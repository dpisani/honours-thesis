function filtered = signal_filter(D, low, high)
  if D.rate == 0,
    filtered = D;
    return;
  end
  [b,a] = butter(4, [low high]/(D.rate/2));
  f = filter(b, a, D.data);
  filtered = struct('data', f, 'rate', D.rate, 'chlabels', {D.chlabels});
