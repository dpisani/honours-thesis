function processed = process_dataset(dataset, fingerprint_inmodel, classify_inmodel)
  result = {};
  fingerprints = [];
  idx = {};
  current_idx = 1;
  for i=1:size(dataset, 2),
    sample = dataset{i};
    features = [];
    targets = to_targets(sample.epoch_score);
    labels = to_labels(sample.epoch_score);
    %filter the signals
    sample.eeg = signal_filter(sample.eeg, 0.5, 30);
    sample.eog = signal_filter(sample.eog, 0.5, 30);
    %sample.emg = signal_filter(sample.emg, 5, 100); sample rate too low!

    for j = 1:size(sample.epoch_score,1)-1,
        features = [features ; epoch_features(sample, j)];
    end

    %select features
    features = features(:, classify_inmodel);

    result{i} = struct('features', features, 'targets', targets, 'labels', {labels});
    idx{i} = [current_idx:(current_idx+size(labels,1)-1)];
    current_idx = current_idx+size(labels,1);
    fingerprints = [fingerprints ; sample_fingerprint(sample)];

    fprintf('.');
  end

  %select fingerprint features
  fingerprints = fingerprints(:, fingerprint_inmodel);

  %normalise the fingerprints
  fp_min = repmat(min(fingerprints), size(fingerprints,1), 1);
  fp_max = repmat(max(fingerprints), size(fingerprints,1), 1);

  fingerprints = (fingerprints - fp_min)./(fp_max - fp_min);


  features_combined = result{1}.features;
  targets_combined = result{1}.targets;
  labels_combined = result{1}.labels;
  for i=2:size(result,2),
    features_combined = [features_combined ; result{i}.features];
    targets_combined = [targets_combined ; result{i}.targets];
    labels_combined = [labels_combined ; result{i}.labels];
  end

  results_combined = struct('features', features_combined, 'targets', targets_combined, 'labels', {labels_combined});

  processed = struct('samples', {result}, 'samples_combined', results_combined, 'fingerprints', fingerprints, 'idx', {idx});
  fprintf(' - done\n');
