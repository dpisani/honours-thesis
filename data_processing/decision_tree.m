ctree = ClassificationTree.fit(features_combined, scores_combined);
cvtree = crossval(ctree);
L = kfoldLoss(cvtree)