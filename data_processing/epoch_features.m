function features = epoch_features(D, epoch)
	% FFT processing for only part of an epoch
	function spectrum = segment_spectrum(data, segment)
		time_seg = 2; %seconds
		a=(segment-1)*time_seg*data.rate+1;
		b=segment*time_seg*data.rate;
		epoch_seg = data.data(a:b, 1:end);
		spectrum = fft_process(epoch_seg, time_seg, data.rate);
	end

	%EEG Features

	%extract the epoch
	epoch_data = extract_epoch(D.eeg, epoch);

	%Time domain features

	%entropy
	SH_ENT = [];
	LO_ENT = [];
	for ch=1:size(epoch_data.data,2),
		SH_ENT = [SH_ENT wentropy(epoch_data.data(:,ch), 'shannon')];
		LO_ENT = [LO_ENT wentropy(epoch_data.data(:,ch), 'log energy')];
	end

	%Standard Deviation
	SD = std(epoch_data.data);

	%skewness
	SKEW = skewness(epoch_data.data);

	%Kurtosis numbers
	KURT = kurtosis(epoch_data.data);

	%Hjorths
	%Activity
	ACT = var(epoch_data.data);
	%Mobility + Complexity
	MOB = [];
	COM = [];

	for ch=1:size(epoch_data.data,2),
		[mobility,complexity] = HjorthParameters(epoch_data.data(:,ch));
		MOB = [MOB mobility];
		COM = [COM complexity];
	end



	%Frequency domain features

	%30 second FFT doesnt give us much. Split the epoch up into 15 parts and average the FFTs of all of them
	Pyy = segment_spectrum(epoch_data, 1);
	time_segment = 2; % our epoch can be throught of as a 2 second one now
	for i=2:15,
		Pyy = Pyy + segment_spectrum(epoch_data, i);
	end
	Pyy = Pyy ./ 15;

%	#extract the range from 0-30 Hz
	freqRange = 30;
	span = band_span(epoch_data.rate, 0, freqRange, time_segment);

%	#Power Spectrum
	PS = sum(Pyy(span(1):span(2), 1:end),1);

	spanA = band_span(epoch_data.rate, 0, 4, time_segment);
	spanB = band_span(epoch_data.rate, 4, 8, time_segment);
	spanC = band_span(epoch_data.rate, 8, 13, time_segment);
	spanD = band_span(epoch_data.rate, 22, 30, time_segment);

%	#Power Ratio
	PRa = sum(Pyy(spanA(1):spanA(2), 1:end),1) ./ PS;
	PRb = sum(Pyy(spanB(1):spanB(2), 1:end),1) ./ PS;
	PRc = sum(Pyy(spanC(1):spanC(2), 1:end),1) ./ PS;
	PRd = sum(Pyy(spanD(1):spanD(2), 1:end),1) ./ PS;

%	#Spectral Frequency
	SF = zeros(1, size(Pyy, 2));
	for i=span(1):span(2),
		freq = 30 * i/span(2);
		SF = SF + Pyy(i, 1:end) * freq;
	end
	SF = SF ./ PS;

	% EMG Features
	if size(D.emg.data, 2) > 0
		% extract the epoch and perform an FFT
		epoch_data = extract_epoch(D.emg, epoch);

		%30 second FFT doesnt give us much. Split the epoch up into 15 parts and average the FFTs of all of them
		Pyy = fft_process(epoch_data.data, 30, epoch_data.rate);

		%	#extract the range from 0-30 Hz
		span = band_span(epoch_data.rate, 0, 30, time_segment);

		%total power
		PS_EMG = sum(Pyy(1:end, 1:end),1);

		%Standard Deviation
		SD_EMG = [std(epoch_data.data)];

		%entropy
		ENT_EMG = [];
		for ch=1:size(epoch_data.data,2),
			ENT_EMG = [ENT_EMG wentropy(epoch_data.data(:,ch), 'shannon')];
		end

		%Kurtosis numbers
		KURT_EMG = kurtosis(epoch_data.data);

	else
		PS_EMG = [];
		SD_EMG = [];
		ENT_EMG = [];
		KURT_EMG = [];
	end

	%EOG Features
	if size(D.eog.data, 2) > 0
		% extract the epoch and perform an FFT
		epoch_data = extract_epoch(D.eog, epoch);

		%30 second FFT doesnt give us much. Split the epoch up into 15 parts and average the FFTs of all of them
		Pyy = segment_spectrum(epoch_data, 1);
		time_segment = 2; % our epoch can be throught of as a 2 second one now
		for i=2:15,
			Pyy = Pyy + segment_spectrum(epoch_data, i);
		end
		Pyy = Pyy ./ 15;

		%relative power
		spanA = band_span(epoch_data.rate, 0, 4, time_segment);
		spanB = band_span(epoch_data.rate, 0, 30, time_segment);
		PR_EOG = sum(Pyy(spanA(1):spanA(2), 1:end),1) ./ sum(Pyy(spanB(1):spanB(2), 1:end),1);

		%Standard Deviation
		SD_EOG = [std(epoch_data.data)];

		%entropy
		ENT_EOG = [];
		for ch=1:size(epoch_data.data,2),
			ENT_EOG = [ENT_EOG wentropy(epoch_data.data(:,ch), 'shannon')];
		end

		%Kurtosis numbers
		KURT_EOG = kurtosis(epoch_data.data);

	else
		PR_EOG = [];
		SD_EOG = [];
		ENT_EOG = [];
		KURT_EOG = [];
	end


	features = [PS, PRa, PRb, PRc, PRd, SF, SH_ENT, LO_ENT, SD, SKEW, KURT, ACT, MOB, COM, PS_EMG, SD_EMG, ENT_EMG, KURT_EMG, PR_EOG, SD_EOG, ENT_EOG, KURT_EOG];

	features(isnan(features)) = 0;

end
