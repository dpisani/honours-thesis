function [] = evaluate_clustering(fingerprints, report_filename, figures_filename_prefix, figures_url_prefix)

  klist = [2:ceil(size(fingerprints, 1)/2)];

  %davies-bouldin index
  eva = evalclusters(fingerprints,'kmeans','DaviesBouldin', 'klist', klist);

  %open file
  fid = fopen(report_filename,'a');
  if fid ~= -1
    %print performance table
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: Davies-Bouldin Index\n');
    fprintf(fid, '  type: table\n');
    fprintf(fid, '  contents:\n');

    fprintf(fid, '   - [%s, %s]\n', 'Optimal K', 'Criterion Value');
    fprintf(fid, '   - [%d, %f]\n', eva.OptimalK, eva.CriterionValues(eva.OptimalK - 1));

    fclose(fid); %# Close the file
  end

  fig = figure, plot(eva);
  print(strcat(figures_filename_prefix, '_DB_clustering'), '-djpeg');
  close(fig);
  write_image_section(report_filename, 'Davies-Bouldin Plot', strcat(figures_url_prefix, '_DB_clustering.jpg'))


  %silhouette index
  eva = evalclusters(fingerprints,'kmeans','silhouette', 'klist', klist);

  %open file
  fid = fopen(report_filename,'a');
  if fid ~= -1
    %print performance table
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: Silhouette Index\n');
    fprintf(fid, '  type: table\n');
    fprintf(fid, '  contents:\n');

    fprintf(fid, '   - [%s, %s]\n', 'Optimal K', 'Criterion Value');
    fprintf(fid, '   - [%d, %f]\n', eva.OptimalK, eva.CriterionValues(eva.OptimalK - 1));

    fclose(fid); %# Close the file
  end

  fig = figure, plot(eva);
  print(strcat(figures_filename_prefix, '_S_clustering'), '-djpeg');
  close(fig);
  write_image_section(report_filename, 'Silhouette Plot', strcat(figures_url_prefix, '_S_clustering.jpg'))

  %Calinski-Harabasz index
  eva = evalclusters(fingerprints,'kmeans','CalinskiHarabasz', 'klist', klist);

  %open file
  fid = fopen(report_filename,'a');
  if fid ~= -1
    %print performance table
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: Calinski-Harabasz Index\n');
    fprintf(fid, '  type: table\n');
    fprintf(fid, '  contents:\n');

    fprintf(fid, '   - [%s, %s]\n', 'Optimal K', 'Criterion Value');
    fprintf(fid, '   - [%d, %f]\n', eva.OptimalK, eva.CriterionValues(eva.OptimalK - 1));

    fclose(fid); %# Close the file
  end

  fig = figure, plot(eva);
  print(strcat(figures_filename_prefix, '_CH_clustering'), '-djpeg');
  close(fig);
  write_image_section(report_filename, 'Calinski-Harabasz Plot', strcat(figures_url_prefix, '_CH_clustering.jpg'))
