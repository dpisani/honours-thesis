function spectrum = epochSpectrum(A, epochIndex)
samples = 30 * A.rate;
epochStart = (epochIndex-1)*samples + 1;
epochEnd = epochIndex*samples;
Y = fft(A.data(epochStart:epochEnd, 1:end))
Pyy = Y.*conj(Y)/samples;
spectrum = Pyy(1:samples/2,1:end);

