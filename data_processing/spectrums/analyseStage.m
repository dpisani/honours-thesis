function analysis = analyseStage(data, label)
analysis = zeros(data.rate*30/2, 6);
count = 0;
for i = 1:data.epoch_numbers(end-1)
	if data.epoch_score{i} == label;
		analysis = analysis + epochSpectrum(data,i);
		count = count + 1
	end
end
if count > 1
	analysis = analysis ./ count;
end
