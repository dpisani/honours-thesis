function k = plotSpectrum(Pyy, freqRange, data)
epochSize=data.rate*30;
freq = data.rate/epochSize;
f=freq*(0:epochSize);
span = freqRange/freq;
plot(f(1:span),Pyy(1:span,1:end));
title('Power Spectral Density');
xlabel('Frequency (Hz)');
legend(data.colheaders);
