% train on one subject and test the classifier on the rest
function [] = nn_one_v_others(featureset, report_filename, figures_filename_prefix, figures_url_prefix)

  startindex = 1;
  bestAcc = 0;
  bestPerf = NaN;
  worstAcc = 1;
  worstPerf = NaN;
  outputs_combined = [];
  targets_combined = [];
  train_t = 0;
  class_t = 0;
  scores = [];
  for s=1:size(featureset.samples,2),
    %get indices of this sample
    sample = featureset.samples{s};
    indlen = size(sample.labels,1);
    testIdx = [];
    if startindex > 1,
      testIdx = [1:(startindex-1)];
    end
    if s < size(featureset.samples,2),
      lastind = size(featureset.samples_combined.labels,1);
      testIdx = [testIdx (startindex+indlen):lastind];
    end
    trainIdx = [startindex:(startindex+indlen-1)];

    startindex = startindex + indlen;

    %run the NN
    [test_outputs, test_labels, train_time, classify_time] = nn_process(featureset.samples_combined, trainIdx, testIdx);
    perfinfo = struct('targets', test_labels, 'outputs', test_outputs, 'train_time', train_time, 'classify_time', classify_time);
    outputs_combined = [outputs_combined test_outputs];
    targets_combined = [targets_combined test_labels];
    train_t = train_t + train_time;
    class_t = class_t + classify_time;

    CP = classperf(perfinfo.targets, perfinfo.outputs);

    scores = [scores CP.CorrectRate];

    if CP.CorrectRate > bestAcc,
      bestAcc = CP.CorrectRate;
      bestPerf = perfinfo;
    end
    if CP.CorrectRate < worstAcc,
      worstAcc = CP.CorrectRate;
      worstPerf = perfinfo;
    end
  end

  write_text_section(report_filename, 'About', 'One subjects sleep was used for training. The remaining nights were used as testing data.');

  allPerf = struct('targets', targets_combined, 'outputs', outputs_combined, 'train_time', train_t / size(featureset.samples,2), 'classify_time', class_t / size(featureset.samples,2));

  write_group_perf(bestPerf, worstPerf, allPerf, scores, report_filename, figures_filename_prefix, figures_url_prefix);
