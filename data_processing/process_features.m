%generate feature sets and target sets
function featuresets = process_features(option)
  if ~exist('option','var'),
    ca_fingeprint_idx = load('f_inmodel_ca.mat');
    car_fingeprint_idx = load('f_inmodel_car.mat');
    se_fingeprint_idx = load('f_inmodel_se.mat');

    ca_classify_idx = load('e_inmodel_ca.mat');
    car_classify_idx = load('e_inmodel_car.mat');
    se_classify_idx = load('e_inmodel_se.mat');
  else
    ca_fingeprint_idx = load('f_inmodel_ca.mat');
    car_fingeprint_idx = load('f_inmodel_car.mat');
    se_fingeprint_idx = load('f_inmodel_se.mat');

    ca_classify_idx = load('e_inmodel_ca.mat');
    car_classify_idx = load('e_inmodel_car.mat');

    if strcmp(option, 'noemg'),
      se_classify_idx = load('e_inmodel_se_no_emg.mat');
    elseif strcmp(option, 'noeog'),
      se_classify_idx = load('e_inmodel_se_no_eog.mat');
    elseif strcmp(option, 'eegonly'),
      se_classify_idx = load('e_inmodel_se_eegonly.mat');
    else
      se_classify_idx = load('e_inmodel_se.mat');
    end
  end

  featuresets = load('processed_features.mat');
  featuresets = featuresets.featuresets;

  function filtered = filter_features(featureset, inmodel, infingerprint)
    filtered = featureset;
    filtered.samples_combined.features = filtered.samples_combined.features(:, inmodel);

    if exist('infingerprint','var'),
      filtered.fingerprints = filtered.fingerprints(:, infingerprint);
    end

    samples = {};

    for s=1:size(filtered.samples,2),
      sample = filtered.samples{s};
      sample.features = sample.features(:, inmodel);
      samples{s} = sample;
    end

    filtered.samples = samples;
  end

  control_apnea = filter_features(featuresets.control_apnea, ca_classify_idx.idx, ca_fingeprint_idx.idx);
  sleep_edfx = filter_features(featuresets.sleep_edfx, se_classify_idx.idx, se_fingeprint_idx.idx);
  control_apnea_reduced = filter_features(featuresets.control_apnea_reduced, car_classify_idx.idx, car_fingeprint_idx.idx);
  sleep_edfx_subjects = filter_features(featuresets.sleep_edfx_subjects, se_classify_idx.idx);

  featuresets = struct('control_apnea', control_apnea, 'sleep_edfx', sleep_edfx, 'control_apnea_reduced', control_apnea_reduced, 'sleep_edfx_subjects', sleep_edfx_subjects);
end
