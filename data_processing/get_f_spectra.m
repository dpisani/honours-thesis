function spectra = cluster_correlation(data)
  s_spectra = {};
  d_spectra = {};
  no_samples = size(data, 2);
  for ii=1:no_samples,
    D = data{ii};
    %break up into 5s segments and combine the fft's
    time_seg = 5;
    segment_size = D.eeg.rate * time_seg; %5 seconds
    no_segments = floor(size(D.eeg.data,1) / segment_size);

    Pyy = [];
    for seg=1:no_segments,
      a = (seg-1)*segment_size + 1;
      b = (seg)*segment_size;
      data_seg = D.eeg.data(a:b, 1:end);
      spec = fft_process(data_seg, time_seg, D.eeg.rate);

      if size(Pyy,2) == 0
        Pyy = spec;
      else
        Pyy = Pyy + spec;
      end
    end

    Pyy = Pyy ./ no_segments;

    spanSpindle = band_span(D.eeg.rate, 8, 16, time_seg);
    spanD = band_span(D.eeg.rate, 0, 4, time_seg);

    s_spectra{ii} = Pyy(spanSpindle(1):spanSpindle(2), 1:end);
    d_spectra{ii} = Pyy(spanD(1):spanD(2), 1:end);
  end

  spectra = struct('s_spectra', {s_spectra}, 'd_spectra', {d_spectra});
