%inputs
%data - data set
%k: number of clusters to make
%#function network
function [] = cluster_train_subjects(featureset, k, report_filename, figures_filename_prefix, figures_url_prefix)
  bestAcc = 0;
  bestPerf = NaN;
  worstAcc = 1;
  worstPerf = NaN;
  outputs_combined = [];
  targets_combined = [];
  result_outputs = {};
  result_targets = {};
  train_t = 0;
  class_t = 0;
  scores = [];

  night1_idx = [1:2:size(featureset.samples,2)];
  night2_idx = [2:2:size(featureset.samples,2)];
  %night 1
  %create clusters
  [c_idx, centroids] = kmeans(featureset.fingerprints(night1_idx, :), k);

  %create classifier for each cluster
  for c=1:k,
    trainIdx = [];
    for ii = 1:size(night1_idx,2),
      if c_idx(ii) == c,
        trainIdx = [trainIdx featureset.idx{night1_idx(ii)}];
      end
    end
    %create a classifier
    x = featureset.samples_combined.features(trainIdx, :);
    t = featureset.samples_combined.targets(trainIdx, :)
    [net, train_time] = train_nn(x', t');
    %test on each sample from night 2
    for s=night2_idx,
      %find closest centroid
      test_fingerprint = featureset.fingerprints(s, :);
      closest_c = 0;
      closest_distance = 0;
      for c=1:k,
        centroid = centroids(c, :);
        dis = pdist([centroid ; test_fingerprint]);
        if closest_c == 0 || closest_distance > dis,
          closest_distance = dis;
          closest_c = c;
        end
      end
      %if this isnt our centroid, exit
      if closest_c ~= c,
        continue;
      end

      testIdx = featureset.idx{s};
      trx = featureset.samples_combined.features(testIdx, :);
      tyx = featureset.samples_combined.targets(testIdx, :);
      tic
      y = net(trx');
      classify_time = toc;
      test_labels = vec2ind(tyx');
      test_outputs = vec2ind(y);

      perfinfo = struct('targets', test_labels, 'outputs', test_outputs, 'train_time', train_time, 'classify_time', classify_time);
      outputs_combined = [outputs_combined test_outputs];
      targets_combined = [targets_combined test_labels];
      result_outputs{s} = test_outputs;
      result_targets{s} = test_labels;
      train_t = train_t + train_time;
      class_t = class_t + classify_time;

      CP = classperf(perfinfo.targets, perfinfo.outputs);

      scores = [scores CP.CorrectRate];

      if CP.CorrectRate > bestAcc,
        bestAcc = CP.CorrectRate;
        bestPerf = perfinfo;
      end
      if CP.CorrectRate < worstAcc,
        worstAcc = CP.CorrectRate;
        worstPerf = perfinfo;
      end
    end
  end

  if exist('figures_filename_prefix','var'),
    write_text_section(report_filename, 'About', 'The first night for all subjects were clustered. The second night was then tested against these clusters.');

    allPerf = struct('targets', targets_combined, 'outputs', outputs_combined, 'train_time', train_t / size(featureset.samples,2), 'classify_time', class_t / size(featureset.samples,2));

    write_group_perf(bestPerf, worstPerf, allPerf, scores, report_filename, figures_filename_prefix, figures_url_prefix);
  else
    output_raw_perf(targets_combined, outputs_combined, report_filename);
    output_individual_raw_perf(result_targets, result_outputs, report_filename);
  end
