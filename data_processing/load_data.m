function datasets = load_data()
  samples = {};
  samples{1} = load('../data/control_apnea/control_apnea_1.mat');
  fprintf('.');
  samples{2} = load('../data/control_apnea/control_apnea_2.mat');
  fprintf('.');
  samples{3} = load('../data/control_apnea/control_apnea_3.mat');
  fprintf('.');
  samples{4} = load('../data/control_apnea/control_apnea_4.mat');
  fprintf('.');
  samples{5} = load('../data/control_apnea/control_apnea_5.mat');
  fprintf('.');
  samples{6} = load('../data/control_apnea/control_apnea_6.mat');
  fprintf('.');
  samples{7} = load('../data/control_apnea/control_apnea_7.mat');
  fprintf('.');
  samples{8} = load('../data/control_apnea/control_apnea_8.mat');
  fprintf('.');
  samples{9} = load('../data/control_apnea/control_apnea_9.mat');
  fprintf('. - done\n');

  control_apnea_data = {};
  for i=1:size(samples,2),
    sample = samples{i};
    epoch_score = sample.epoch_score;

    eeg = struct('chlabels', {sample.colheaders}, 'rate', sample.rate, 'data', sample.data);
    emg = struct('chlabels', {{}}, 'rate', 0, 'data', []);
    eog = struct('chlabels', {{}}, 'rate', 0, 'data', []);
    control_apnea_data{i} = struct('eeg', eeg, 'emg', emg, 'eog', eog, 'epoch_score', {epoch_score});
  end

  control_apnea_reduced_data = {};
  for i=1:size(samples,2),
    sample = samples{i};
    epoch_score = sample.epoch_score;
    reduced_data = sample.data(1:end, [3 5]);
    offset_data = sample.data(1:end, [4 6]);
    eeg = struct('chlabels', {{'Fz-Cz','Pz-Oz'}}, 'rate', sample.rate, 'data', reduced_data - offset_data);
    emg = struct('chlabels', {{}}, 'rate', 0, 'data', []);
    eog = struct('chlabels', {{}}, 'rate', 0, 'data', []);
    control_apnea_reduced_data{i} = struct('eeg', eeg, 'emg', emg, 'eog', eog, 'epoch_score', {epoch_score});
  end

  %load and process sleep-edfx data
  %filenames for data are specified in 2 special files
  sleep_edfx_data = {};

  base_path = '../data/sleep-edfx/';
  fpsg = fopen('../data/SC_PSG');
  fhyp = fopen('../data/SC_HYP');

  lpsg = fgetl(fpsg);
  lhyp = fgetl(fhyp);
  no_samples = 0;
  while ischar(lpsg),
    [d, h] = readEDF(strcat(base_path, lpsg));
    [a, b] = readEDF(strcat(base_path, lhyp));

    no_samples = no_samples + 1;
    %generate list of labels
    epoch_score = {};
    epochs = 0;
    first_sleep = 1;
    for ii=1:size(b.annotation.duration, 1),
      duration = b.annotation.duration(ii);
      %the epoch label is the last character
      score = b.annotation.event{ii}(end);
      if strcmp(b.annotation.event{ii}, 'Movement time'),
          score = 'M';
      end
      if score == 'M' || score == '?',
        continue;
      end
      while duration > 0,
        epochs = epochs + 1;
        epoch_score{epochs} = score;
        duration = duration - 30; %each epoch goes for 30 seconds

        if first_sleep == 1 && score ~= 'W',
          first_sleep = epochs;
        end
      end
    end

    %retain 5 minutes of sleep
    start_epoch = max([first_sleep - 10, 1]);

    epoch_score = transpose(epoch_score);
    epoch_score = epoch_score(start_epoch:end);

    eeg_data = [d{1}, d{2}];
    emg_data = [d{5}];
    eog_data = [d{3}];
    %trim data
    %the samplerate in the data is given as samples/epoch
    start_datapoint = ((start_epoch-1) * h.samplerate(1)) + 1;
    eeg_data = eeg_data(start_datapoint:end, 1:end);

    start_datapoint = ((start_epoch-1) * h.samplerate(5)) + 1;
    emg_data = emg_data(start_datapoint:end, 1:end);

    start_datapoint = ((start_epoch-1) * h.samplerate(3)) + 1;
    eog_data = eog_data(start_datapoint:end, 1:end);

    eeg = struct('chlabels', {h.labels(1:2)}, 'rate', h.samplerate(1) / 30, 'data', eeg_data);
    emg = struct('chlabels', {h.labels(5)}, 'rate', h.samplerate(5) / 30, 'data', emg_data);
    eog = struct('chlabels', {h.labels(3)}, 'rate', h.samplerate(3) / 30, 'data', eog_data);
    sleep_edfx_data{no_samples} = struct('eeg', eeg, 'emg', emg, 'eog', eog, 'epoch_score', {epoch_score});

    %get next data set
    lpsg = fgetl(fpsg);
    lhyp = fgetl(fhyp);
    fprintf('.');
  end
  fprintf(' - done\n');

  %return the data
  datasets = struct('control_apnea', {control_apnea_data}, 'sleep_edfx', {sleep_edfx_data}, 'control_apnea_reduced', {control_apnea_reduced_data});
