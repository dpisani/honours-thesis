function [] = output_raw_perf(targets, outputs, train_time, classify_time, name)
  data = struct('targets', targets, 'outputs', outputs, 'train_time', train_time, 'classify_time', classify_time);
  filename = strcat('results/', name, '_results_all', '.mat');
  save(filename, 'data');
