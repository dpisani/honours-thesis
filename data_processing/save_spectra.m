data = load_data;

spectra = get_f_spectra(data.sleep_edfx);
save 'spec_se' spectra;

spectra = get_f_spectra(data.control_apnea);
save 'spec_ca' spectra;
