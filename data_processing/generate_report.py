import time, subprocess, os

#only run the tests if matlab scripts have changed
commit_files = subprocess.check_output(["git", "show","--pretty=format:", "--name-only"]).strip()

if "data_processing" in commit_files:
  commit_message = subprocess.check_output(["git", "log","-1", "--pretty=%B"]).strip()
  commit_hash = subprocess.check_output(["git", "log","-1", "--pretty=%H"]).strip()

  frontmatter = """---
layout: result_generated
title: "Test Results"
categories: "results"
date: """ + time.strftime('%Y-%m-%d %H:%M:%S') + """
message: """ + '\"' + commit_message + """"
hash: """ + '\"' + commit_hash + """"
tests:
"""

  filename = 'results/reports/_posts/' + time.strftime('%Y-%m-%d') + "-" + commit_hash + ".markdown";
  f = open(filename, 'w')
  f.write(frontmatter);

  #close file and give it to matlab to use
  f.flush()
  f.close()
  image_filepath = "results/reports/images/" + commit_hash
  image_url = "/images/" + commit_hash
  subprocess.call(["matlab", "-nodisplay", "-nosplash", "-r", "run_tests(" + "\'" + filename + "\'" + ", " + "\'" + image_filepath + "\'" + ", " + "\'" + image_url + "\'" + ")"])

  #open the file again
  f = open(filename, 'a')
  #close the YAML
  f.write("\n---\n")
  #add a diff at the bottom
  f.write("\n##Changes##\n")
  f.write("\n{% highlight diff %}\n")
  f.write("\n{% raw %}\n")
  f.write(subprocess.check_output(["git", "diff", "HEAD^", "HEAD"]).strip())
  f.write("\n\n{% endraw %}\n")
  f.write("\n{% endhighlight %}\n")

  f.flush()
  os.fsync(f.fileno())
  f.close
