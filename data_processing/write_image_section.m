function [] = write_text_section(report_filename, heading, url)
  %open file
  fid = fopen(report_filename,'a');
  if fid ~= -1
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: "%s"\n', heading);
    fprintf(fid, '  type: img\n');
    fprintf(fid, '  url: "%s"\n', url);

    fclose(fid); %# Close the file
  end
