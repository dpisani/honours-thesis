function [] = write_group_perf(bestPerf, worstPerf, allPerf, scores, report_filename, figures_filename_prefix, figures_url_prefix)
  write_text_section(report_filename, 'Best', 'Below is the subject who tested with best accuracy');
  write_performance(bestPerf, report_filename, strcat(figures_filename_prefix, '_best'), strcat(figures_url_prefix, '_best'));

  write_text_section(report_filename, 'Worst', 'Below is the subject who tested with worst accuracy');
  write_performance(worstPerf, report_filename, strcat(figures_filename_prefix, '_worst'), strcat(figures_url_prefix, '_worst'));

  write_text_section(report_filename, 'Average', 'Below is the aggregation of the performace of all subjects');
  write_performance(allPerf, report_filename, strcat(figures_filename_prefix, '_avg'), strcat(figures_url_prefix, '_avg'));

  %open file
  fid = fopen(report_filename,'a');
  if fid ~= -1
    %print performance table
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: Group Performance\n');
    fprintf(fid, '  type: table\n');
    fprintf(fid, '  contents:\n');
    fprintf(fid, '   - [%s, %f%%]\n', 'Mean', mean(scores)*100);
    fprintf(fid, '   - [%s, %f%%]\n', 'Standard Deviation', std(scores)*100);
    fprintf(fid, '   - [%s, %f%%]\n', 'Best Performance', max(scores)*100);
    fprintf(fid, '   - [%s, %f%%]\n', 'Worst Performance', min(scores)*100);
    fclose(fid); %# Close the file
  end

  fig = figure, bar(sort(scores));
  title('Accuracy per subject');
  axis([-inf inf 0 1]);
  print(strcat(figures_filename_prefix, '_accuracy_per_subject'), '-djpeg');
  close(fig);
  write_image_section(report_filename, 'All Subjects', strcat(figures_url_prefix, '_accuracy_per_subject.jpg'));
