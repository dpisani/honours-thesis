function [] = plot_fingerprints(data, fingerprints, report_filename, figures_filename_prefix, figures_url_prefix)
  no_samples = size(data, 2);
  fig = figure;
  charts = 1;
  figs = 0;

  %create clusters
  k = 3;
  [c_idx, centroids] = kmeans(fingerprints, k);

  for ii=1:no_samples,
    D = data{ii};
    %break up into 5s segments and combine the fft's
    time_seg = 5;
    segment_size = D.eeg.rate * time_seg; %5 seconds
    no_segments = floor(size(D.eeg.data,1) / segment_size);

    Pyy = [];
    for seg=1:no_segments,
      a = (seg-1)*segment_size + 1;
      b = (seg)*segment_size;
      data_seg = D.eeg.data(a:b, 1:end);
      spec = fft_process(data_seg, time_seg, D.eeg.rate);

      if size(Pyy,2) == 0
        Pyy = spec;
      else
        Pyy = Pyy + spec;
      end
    end

    Pyy = Pyy ./ no_segments;

    spanD = band_span(D.eeg.rate, 0, 4, time_seg);

    subplot(3, 2, figs+1);
    x_ax = linspace(0,4,spanD(2)-spanD(1)+1);
    plot(x_ax, Pyy(spanD(1):spanD(2), 1:end));
    title(strcat('Cluster: ', num2str(c_idx(ii))));

    figs = figs + 1;
    if figs == 6,
      figs = 0;
      charts = charts + 1;
      print(strcat(figures_filename_prefix, '_fingerprints', num2str(charts)), '-djpeg');
      close(fig);
      fig = figure;
      write_image_section(report_filename, 'Subject Fingerprints', strcat(figures_url_prefix, '_fingerprints', num2str(charts), '.jpg'));
    end

  end
  if figs > 0,
    charts = charts + 1;
    print(strcat(figures_filename_prefix, '_fingerprints', num2str(charts)), '-djpeg');
    write_image_section(report_filename, 'Subject Fingerprints', strcat(figures_url_prefix, '_fingerprints', num2str(charts), '.jpg'));
  end
  close(fig);
