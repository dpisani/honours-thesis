function corr = night_correlation(data, spectra)
  no_samples = size(data, 2);
  charts = 1;
  figs = 0;

  corr = {};


  for n=1:no_samples,
    others = {};
    for m=1:no_samples,
      Fs = data{1}.eeg.rate;
      x = data{n}.eeg.data;
      y = data{m}.eeg.data;
      l = min(length(x), length(y));
      Cxy = mscohere(x(1:l,:),y(1:l,:),[],[],[],Fs);
      tothz = Fs/2;
      hz1 = length(Cxy) / tothz;
      hz8 = floor(hz1*8)+1;
      hz16 = floor(hz1*16)+1;
      focus = Cxy(hz8:hz16,:);
      meancohere = mean(mean(focus));

      sx = spectra.s_spectra{n};
      sy = spectra.s_spectra{m};
      frec = 0;
      for c=1:size(sx,2),
        sxc = sx(:, c);
        syc = sy(:, c);
        z = linspace(1, length(sxc), length(sxc));
        z = z';
        frec = frec + frechet(z, sxc, z, syc);
      end

      frec = frec / size(sx,2);

      mserr = mean((sx - sy).^2);

      cperf = struct('msc', meancohere, 'frechet', frec, 'mserr', mserr);
      others{m} = cperf;
    end
    corr{n} = others;
  end
