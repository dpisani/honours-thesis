for night=1:36
spanSpindle = band_span(100, 0, 4, 5);
x_ax = linspace(8,16,spanSpindle(2)-spanSpindle(1)+1);
plot(x_ax, spectra.d_spectra{night})
ylabel('Power')
title('Delta Power Spectrum')
%legend('C3','C4','Fz','Cz','Pz','Oz', 'Location','northeast')
legend('Fpz-Cz', 'Pz-Oz', 'Location','northeast')
xlabel('Frequency(Hz)')
hFig = figure(1);
set(hFig, 'Position', [0 0 400 300])
set(gcf,'PaperUnits','inches','PaperPosition',[0 0 4 3])
print(strcat('../thesis/images/appendix/SE_del_', num2str(night)), '-djpeg');
end