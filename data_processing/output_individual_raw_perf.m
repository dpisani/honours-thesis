function [] = output_individual_raw_perf(targets, outputs, train_times, classify_times, name)
  data = {};
  for k = 1:length(targets),
    d = struct('targets', targets{k}, 'outputs', outputs{k}, 'train_time', train_times{k}, 'classify_time', classify_times{k});
    data{k} = d;
  end

  filename = strcat('results/',name,'_results_indiv', '.mat');
  save(filename, 'data');
