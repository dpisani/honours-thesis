function targets = to_labels( epoch_scores )
%TO_LABELS Summary of this function goes here
%   Detailed explanation goes here
    targets = {};
    for i=1:size(epoch_scores)-1,
        target = epoch_scores{i};
        targets = [targets; target];
    end
end
