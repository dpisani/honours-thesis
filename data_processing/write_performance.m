function [] = write_performance(perfinfo, report_filename, figures_filename_prefix, figures_url_prefix)

  % Plots
  t_vec = ind2vec(perfinfo.targets);
  o_vec = ind2vec(perfinfo.outputs);

  if size(o_vec, 1) < size(t_vec, 1),
    %add extra columns
    o_vec(size(t_vec, 1), size(t_vec, 2)) = 0;
  end

  if size(o_vec, 1) > size(t_vec, 1),
    %add extra columns
    t_vec(size(o_vec, 1), size(o_vec, 2)) = 0;
  end

  fig = figure, plotconfusion(t_vec,o_vec);
  set(gca,'xticklabel',{'W' 'S1' 'S2' 'S3' 'S4' 'REM' 'All'});
  set(gca,'yticklabel',{'W' 'S1' 'S2' 'S3' 'S4' 'REM' 'All'});
  title('Test Confusion');
  print(strcat(figures_filename_prefix, '_test_confusion'), '-djpeg');
  close(fig);

  CP = classperf(perfinfo.targets, perfinfo.outputs);

  fig = figure, bar(CP.ErrorDistributionByClass ./ CP.SampleDistributionByClass);
  title('Errors per Class');
  axis([-inf inf 0 1]);
  set(gca,'xticklabel',{'W' 'S1' 'S2' 'S3' 'S4' 'REM'});
  print(strcat(figures_filename_prefix, '_errors_per_class'), '-djpeg');
  close(fig);

  %kappa coefficient
  k_table = zeros(6,6);
  for ex=1:size(perfinfo.targets, 2),
    a = perfinfo.targets(ex);
    b = perfinfo.outputs(ex);
    k_table(a,b) = k_table(a,b) + 1;
  end
  k_table = k_table ./ size(perfinfo.targets, 2);

  Po = 0;
  for ind=1:6,
    Po = Po + k_table(ind,ind);
  end

  Pe = 0;
  for ind=1:6,
    col_v = 0;
    for col=1:6,
      col_v = col_v + k_table(ind, col);
    end
    row_v = 0;
    for row=1:6,
      row_v = row_v + k_table(row, ind);
    end

    Pe = Pe + (col_v * row_v);
  end

  kappa = (Po - Pe) / (1 - Pe);

  %open file
  fid = fopen(report_filename,'a');
  if fid ~= -1
    %print performance table
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: Classifier Performance\n');
    fprintf(fid, '  type: table\n');
    fprintf(fid, '  contents:\n');
    fprintf(fid, '   - [%s, %f%%]\n', 'Correct Rate', CP.CorrectRate * 100);
    fprintf(fid, '   - [%s, %f%%]\n', 'Error Rate', CP.ErrorRate * 100);
    fprintf(fid, '   - [%s, %f%%]\n', 'Sensitivity', CP.Sensitivity * 100);
    fprintf(fid, '   - [%s, %f%%]\n', 'Specificity', CP.Specificity * 100);
    fprintf(fid, '   - [%s, %f]\n', 'Cohen''s Kappa Coefficient', kappa);
    fprintf(fid, '   - [%s, %fs]\n', 'Mean Training time', perfinfo.train_time);
    fprintf(fid, '   - [%s, %fs]\n', 'Mean Classification time', perfinfo.classify_time);

    %insert charts
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: Confusion Matrix\n');
    fprintf(fid, '  type: img\n');
    fprintf(fid, '  url: "%s%s"\n', figures_url_prefix, '_test_confusion.jpg');

    fclose(fid); %# Close the file
  end

  write_image_section(report_filename, 'Error Distribution Per Class', strcat(figures_url_prefix, '_errors_per_class.jpg'))
