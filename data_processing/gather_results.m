for n=11:100,
  disp('Processing Data');
  featuresets = process_features();

  disp('Running Tests');

  % test_postfix = strcat('CA_10fold_n', num2str(n));
  % nn_cross_validate(featuresets.control_apnea.samples_combined, test_postfix);
  %
  test_postfix = strcat('CA_k1_n', num2str(n));
  nn_others_v_one(featuresets.control_apnea, test_postfix);

  % test_postfix = strcat('CA_k4_n', num2str(n));
  % cluster_train(featuresets.control_apnea, 4, test_postfix);
  %
  % test_postfix = strcat('CA_k4_random_n', num2str(n));
  % random_cluster_train(featuresets.control_apnea, 4, test_postfix);

  % test_postfix = strcat('CA_sub_dep_n', num2str(n));
  % subject_dependant_classifier(featuresets.control_apnea, test_postfix);
  %
  %
  % test_postfix = strcat('SE_10fold_n', num2str(n));
  % nn_cross_validate(featuresets.sleep_edfx.samples_combined, test_postfix);
  %
  test_postfix = strcat('SE_k1_n', num2str(n));
  nn_others_v_one(featuresets.sleep_edfx, test_postfix);

  % test_postfix = strcat('SE_k4_n', num2str(n));
  % cluster_train(featuresets.sleep_edfx, 4, test_postfix);
  %
  % test_postfix = strcat('SE_k4_random_n', num2str(n));
  % random_cluster_train(featuresets.sleep_edfx, 4, test_postfix);

  % test_postfix = strcat('SE_sub_dep_merged_n', num2str(n));
  % subject_dependant_classifier(featuresets.sleep_edfx_subjects, test_postfix);
  %
  % test_postfix = strcat('SE_sub_dep_n', num2str(n));
  % subject_dependant_classifier(featuresets.sleep_edfx, test_postfix);



  %
  % disp('Processing Data');
  % featuresets = process_features('noemg');
  %
  % disp('Running Tests');
  %
  % % test_postfix = strcat('SE_10fold_noemg_n', num2str(n));
  % % nn_cross_validate(featuresets.sleep_edfx.samples_combined, test_postfix);
  % %
  % % test_postfix = strcat('SE_k1_noemg_n', num2str(n));
  % % nn_others_v_one(featuresets.sleep_edfx, test_postfix);
  %
  % test_postfix = strcat('SE_k4_noemg_n', num2str(n));
  % cluster_train(featuresets.sleep_edfx, 4, test_postfix);
  %
  % test_postfix = strcat('SE_k4_random_noemg_n', num2str(n));
  % random_cluster_train(featuresets.sleep_edfx, 4, test_postfix);
  %
  % % test_postfix = strcat('SE_sub_dep_merged_noemg_n', num2str(n));
  % % subject_dependant_classifier(featuresets.sleep_edfx_subjects, test_postfix);
  % %
  % % test_postfix = strcat('SE_sub_dep_noemg_n', num2str(n));
  % % subject_dependant_classifier(featuresets.sleep_edfx, test_postfix);
  %
  %
  %
  %
  % disp('Processing Data');
  % featuresets = process_features('noeog');
  %
  % disp('Running Tests');
  %
  % % test_postfix = strcat('SE_10fold_noeog_n', num2str(n));
  % % nn_cross_validate(featuresets.sleep_edfx.samples_combined, test_postfix);
  % %
  % % test_postfix = strcat('SE_k1_noeog_n', num2str(n));
  % % nn_others_v_one(featuresets.sleep_edfx, test_postfix);
  %
  % test_postfix = strcat('SE_k4_noeog_n', num2str(n));
  % cluster_train(featuresets.sleep_edfx, 4, test_postfix);
  %
  % test_postfix = strcat('SE_k4_random_noeog_n', num2str(n));
  % random_cluster_train(featuresets.sleep_edfx, 4, test_postfix);
  %
  % % test_postfix = strcat('SE_sub_dep_merged_noeog_n', num2str(n));
  % % subject_dependant_classifier(featuresets.sleep_edfx_subjects, test_postfix);
  % %
  % % test_postfix = strcat('SE_sub_dep_noeog_n', num2str(n));
  % % subject_dependant_classifier(featuresets.sleep_edfx, test_postfix);
  %
  %
  %
  % disp('Processing Data');
  % featuresets = process_features('eegonly');
  %
  % disp('Running Tests');
  % %
  % % test_postfix = strcat('SE_10fold_eegonly_n', num2str(n));
  % % nn_cross_validate(featuresets.sleep_edfx.samples_combined, test_postfix);
  % %
  % % test_postfix = strcat('SE_k1_eegonly_n', num2str(n));
  % % nn_others_v_one(featuresets.sleep_edfx, test_postfix);
  %
  % test_postfix = strcat('SE_k4_eegonly_n', num2str(n));
  % cluster_train(featuresets.sleep_edfx, 4, test_postfix);
  %
  % test_postfix = strcat('SE_k4_random_eegonly_n', num2str(n));
  % random_cluster_train(featuresets.sleep_edfx, 4, test_postfix);
  %
  % % test_postfix = strcat('SE_sub_dep_merged_eegonly_n', num2str(n));
  % % subject_dependant_classifier(featuresets.sleep_edfx_subjects, test_postfix);
  % %
  % % test_postfix = strcat('SE_sub_dep_eegonly_n', num2str(n));
  % % subject_dependant_classifier(featuresets.sleep_edfx, test_postfix);
end
