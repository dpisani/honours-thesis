
featuresets = process_features(load_data);

%epoch
% idx = select_epoch_features(featuresets.control_apnea)
% save 'e_inmodel_ca_b.mat' idx;
%
% idx = select_epoch_features(featuresets.control_apnea_reduced)
% save 'e_inmodel_car_b.mat' idx;
%
 idx = select_epoch_features(featuresets.sleep_edfx)
 save 'e_inmodel_se.mat' idx;

%fingerprint
% idx = select_fingerprint_features(featuresets.control_apnea)
% save 'f_inmodel_ca_b.mat' idx ;
%
% idx = select_fingerprint_features(featuresets.control_apnea_reduced)
% save 'f_inmodel_car_b.mat' idx ;
%
% idx = select_fingerprint_features(featuresets.sleep_edfx)
% save 'f_inmodel_se_b.mat' idx;

%no eog
%idx = select_epoch_features(featuresets.sleep_edfx)
%save 'e_inmodel_se_no_eog.mat' idx;
