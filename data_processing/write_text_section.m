function [] = write_text_section(report_filename, heading, text_c)
  %open file
  fid = fopen(report_filename,'a');
  if fid ~= -1
    fprintf(fid, ' -\n');
    fprintf(fid, '  title: "%s"\n', heading);
    fprintf(fid, '  type: text\n');
    fprintf(fid, '  text: "%s"\n', text_c);

    fclose(fid); %# Close the file
  end
