function [net, train_time] = train_nn(x, t)
  %time how long it takes to train
  tic

  % Create a Pattern Recognition Network
  hiddenLayerSize = 10;
  net = patternnet(hiddenLayerSize);

  % Choose Input and Output Pre/Post-Processing Functions
  % For a list of all processing functions type: help nnprocess
  %net.input.processFcns = {'removeconstantrows','mapminmax'};
  % This ended up causing S1 to be misclassified
  %net.output.processFcns = {'removeconstantrows','mapminmax'};


  % Setup Division of Data for Training, Validation, Testing
  % For a list of all data division functions type: help nndivide
  net.divideFcn = 'dividerand';  % Divide data randomly
  net.divideMode = 'sample';  % Divide up every sample
  net.divideParam.trainRatio = 2/3;
  net.divideParam.valRatio = 1/3;
  net.divideParam.testRatio = 0/100;

  % For help on training function 'trainscg' type: help trainscg
  % For a list of all training functions type: help nntrain
  net.trainFcn = 'trainscg';  % Scaled conjugate gradient

  % Choose a Performance Function
  % For a list of all performance functions type: help nnperformance
  net.performFcn = 'crossentropy';  % Cross-entropy

  % Choose Plot Functions
  % For a list of all plot functions type: help nnplot
  net.plotFcns = {'plotperform','plottrainstate','ploterrhist', ...
    'plotregression', 'plotfit'};


  % Train the Network
  [net,tr] = train(net,x,t,'useParallel','yes');

  train_time = toc;
